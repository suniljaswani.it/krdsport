<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table){
            $table->increments('id');
            $table->string('firstname', 50);
            $table->string('father_name', 50);
            $table->string('lastname', 50);
            $table->string('email', 100)->nullable()->unique();
            $table->date('dob')->nullable();
            $table->tinyInteger('gender')->comment('1: male, 2: female')->nullable();
            $table->string('mobile', 50)->unique();
            $table->string('password')->nullable();
            $table->string('aadhar_number', 100)->nullable()->unique();
            $table->tinyInteger('eligibility_status')->default(2)->comment('1: Eligible, 2: Not Eligible');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('players');
    }
}
