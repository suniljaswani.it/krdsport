<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->unsigned();
            $table->integer('game_id')->unsigned();
            $table->tinyInteger('gender')->comment('1: Male, 2: Female')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1: Pending, 2: Approved, 3: Rejected');
            $table->tinyInteger('final_eligibility_status')->default(2)->comment('1: Eligible, 2: Not Eligible');
            $table->text('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('game_id')->references('id')->on('games');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_participants');
    }
}
