<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_educations', function (Blueprint $table){
            $table->increments('id');
            $table->integer('player_id')->unsigned();
            $table->date('ssc_pass_year')->nullable();
            $table->date('hsc_pass_year')->nullable();
            $table->date('college_started_year')->nullable();
            $table->tinyInteger('current_college_year')->nullable();
            $table->tinyInteger('total_college_year')->nullable();
            $table->string('current_roll_no')->nullable();
            $table->string('college_stream')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('player_id')->references('id')->on('players');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('player_educations');
    }
}
