<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Sofa\Eloquence\Eloquence;

use App\Models\PlayerEducation;
use App\Models\PlayerDocument;


class Player extends Model
{
    use Eloquence;
    
    protected $fillable = ['firstname', 'father_name', 'lastname', 'email', 'dob', 'gender', 'mobile', 'password', 'aadhar_number', 'eligibility_status'];

    protected $hidden = ['password'];
    
    // --- Relations --- //

    public function player_education()
    {
        return $this->hasOne(PlayerEducation::class);
    }

    public function player_documents()
    {
        return $this->hasMany(PlayerDocument::class);
    }


}
