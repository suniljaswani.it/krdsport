<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use App\Models\Player;
use App\Models\PlayerDocument;
use App\Models\DocumentType;


class PlayerDocument extends Model
{
    use Eloquence;

    const PENDING = 1, APPROVED = 2, REJECTED = 3;

    protected $fillable = ['player_id', 'document_type_id', 'image', 'status', 'remarks'];

    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }



}
