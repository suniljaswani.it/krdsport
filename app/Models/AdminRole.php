<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    protected $fillable = ['admin_id', 'route'];

    public static function openRoutes()
    {
        return [
            null,
            'admin-login',
            'admin-logout',
            'admin-profile',
            'admin-dashboard',
            'admin-access-denied'

        ];
    }
}
