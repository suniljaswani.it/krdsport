<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use Session;


class PlayerEducation extends Model
{
    protected $fillable = ['player_id', 'ssc_pass_year', 'hsc_pass_year', 'college_started_year', 'current_college_year', 'total_college_year', 'current_roll_no', 'college_stream'];

    // Check Player Eligibility
    public static function checkEligibility()
    {
        $player_education = PlayerEducation::where('player_id', Session::get('player')->id)->first();
        
        $current_start_date = Carbon::now()->year.'-06-01';
        $current_end_date = (Carbon::now()->year+1).'-04-30';
        $today = Carbon::now()->format('Y-m-d');;

        $course = $player_education->total_college_year;
        
        if($course == 5)
        {
            if( ($today > $current_start_date) && ($today < $current_end_date) )
            {
                if(Carbon::parse($player_education->hsc_pass_year)->format('Y') == env('PLAYER_PG_YEAR'))
                {
                    return true;
                }
                else 
                {
                    return false;
                }

            }

        }
        else
        {
            if (($today > $current_start_date) && ($today < $current_end_date)) 
            {
                if (Carbon::parse($player_education->hsc_pass_year)->format('Y') == env('PLAYER_UG_YEAR')) 
                {
                    return true;
                } 
                else 
                {
                    return false;
                }

            }

        }

    }

}

