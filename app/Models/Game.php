<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Sofa\Eloquence\Eloquence;

// Models
use App\Models\GameType;

class Game extends Model
{
    use Eloquence;

    protected $fillable = ['name', 'game_type_id', 'description', 'status', 'start_date', 'end_date'];

    const ACTIVE = 1, INACTIVE = 2;

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status', 1)->whereDate('start_date', '<=', date("Y-m-d"))->whereDate('end_date', '>=', date("Y-m-d"));
    }

    // --- Relations --- //
    public function gameType()
    {
        return $this->belongsTo(GameType::class);
    }
    

}
