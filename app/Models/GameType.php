<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Sofa\Eloquence\Eloquence;

class GameType extends Model
{
    use Eloquence;

    protected $fillable = ['name', 'status'];

    const ACTIVE = 1, INACTIVE = 2;

    // Scopes
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    
}
