<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Sofa\Eloquence\Eloquence;

// Models
use App\Models\Player;
use App\Models\Game;

class GameParticipant extends Model
{
    use Eloquence;

    const PENDING = 1, APPROVED = 2, REJECTED = 3;

    protected $fillable = ['player_id', 'game_id', 'gender', 'status', 'final_eligibility_status', 'remarks'];

    //--- Relations ---//
    public function player()
    {
        return $this->belongsTo(Player::class);
    } 

    public function game()
    {
        return $this->belongsTo(Game::class);
    } 



}
