<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Excel;
use Carbon\Carbon;

// Models
use App\Models\GameParticipant;
use App\Models\Player;
use App\Models\Game;


class ApplyController extends Controller
{
    public function index(Request $request)
    {
      
        $participated_players = GameParticipant::with(['player.player_education','game'])
            ->search($request->search, [
                'player.firstname', 'player.lastname', 'player.email', 'player.aadhar_number', 'player.mobile' 
        ]);
        
        if($request->daterange)
        {
            $dates = explode('-', $request->daterange);
            $start_date = Carbon::parse(trim($dates[0]));
            $end_date = Carbon::parse(trim($dates[1]));

            $participated_players = $participated_players->where([
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $start_date == $end_date ? $end_date->addDay(1) : $end_date]
            ]);
        }

        if ($request->game_id) {
            $participated_players->where('game_id', $request->game_id);
        }

        if ($request->status) {
            $participated_players = $participated_players->where('status', $request->status);
        }

        if ($request->gender) {
            $participated_players = $participated_players->where('gender', $request->gender);
        }
        

        if ($request->final_eligibility_status) {
            $participated_players = $participated_players->where('final_eligibility_status', $request->final_eligibility_status);
        }

        if($request->download == 'yes')
        {
            Excel::create('Paticipated Players', function($excel) use ($participated_players, $request) {

                $excel->sheet('Sheet1', function($sheet) use ($participated_players, $request) {

                    if(!empty($request->daterange) || !empty($request->search) || !empty($request->game_id) || !empty($request->status) || !empty($request->gender) || !empty($request->final_eligibility_status))
                    {
                        $participated_players = $participated_players->orderBy('created_at','desc')->get();
                    }
                    else
                    {
                        $participated_players = $participated_players->orderBy('id', 'desc')->paginate(50);
                    }
                    $data = $participated_players->map( function($participated_player){

                        return [
                            'Game Name' => $participated_player->game->name,
                            'Applied Date' => Carbon::parse($participated_player->created_at)->format('d M Y'),
                            'Player Name' => $participated_player->player->firstname.' '.$participated_player->player->lastname,
                            'gender' => $participated_player->gender == 1 ? 'Male' : 'Female',
                            'Mobile' => $participated_player->player->mobile,
                            'Aadhar Number' => $participated_player->player->aadhar_number,
                            'Current Roll Number' => $participated_player->player->player_education->current_roll_no,
                            'College Stream' => $participated_player->player->player_education->college_stream,
                            'Status' => $participated_player->status,
                            'Status Information' => '1 => Pending, 2 => Approved, 3 => Rejected',
                            'Final Eligibility Status' => $participated_player->final_eligibility_status == 1 ? 'Eligible' : 'Not Eligible',
                            'Remarks' => $participated_player->remarks,
                        ];
                    })->toArray();
                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.apply.view',[
            'participated_players' => $participated_players->orderBy('id','desc')->paginate(50),
            'games' => Game::get(),
        ]);
    }

    public function edit(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'status' => 'required',
                'final_eligibility_status' => 'required',
            ], [
                'status.required' => 'Status is Required',
                'final_eligibility_status.required' => 'Final Eligibility Status is Required',
            ]);

            $status = GameParticipant::find($request->id);
            $status->status                     = $request->status;
            $status->final_eligibility_status   = $request->final_eligibility_status;
            $status->gender                     = $request->gender;
            $status->remarks                    = $request->remarks;
            $status->save();

            return redirect()->route('admin-apply-list')->with(['success' => 'Player Status has been updated']);
        }

        $participated_player = GameParticipant::with(['player.player_education', 'game'])->find($request->id);

        return view('admin.apply.edit', [
            'participated_player' => $participated_player,
        ]);
    }

    // public function view_account($id)
    // {
    //     if(!$user = User::whereId($id)->first())
    //         return redirect()->back()->with(['error' => 'Invalid User Details for View']);

    //     return redirect()->route('user-admin-view-access', ['id' => $user->id]);

    // }


}
