<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Excel;
use Carbon\Carbon;

// Models
use App\Models\Player;
use App\Models\PlayerDocument;
use App\Models\PlayerEducation;

class PlayerController extends Controller
{
    public function index(Request $request)
    {
      
        $players = Player::with(['player_education'])
            ->search($request->search, [
                'firstname', 'lastname', 'email', 'aadhar_number', 'player_education.college_stream', 'player_education.current_roll_no'
        ]);
        
        if($request->daterange)
        {
            $dates = explode('-', $request->daterange);
            $start_date = Carbon::parse(trim($dates[0]));
            $end_date = Carbon::parse(trim($dates[1]));

            $players = $players->where([
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $start_date == $end_date ? $end_date->addDay(1) : $end_date]
            ]);
        }

        if ($request->eligibility_status) {
            $players = $players->where('eligibility_status', $request->eligibility_status);
        }

        if($request->download == 'yes')
        {
            Excel::create('Players', function($excel) use ($players, $request) {

                $excel->sheet('Sheet1', function($sheet) use ($players, $request) {

                    if(!empty($request->daterange) || !empty($request->search) || !empty($request->package))
                    {
                        $players = $players->orderBy('created_at','desc')->get();
                    }
                    else
                    {
                        $players = $players->orderBy('id', 'desc')->paginate(50);
                    }
                    $data = $players->map( function($player){

                        return [
                            'Joining Date' => Carbon::parse($player->created_at)->format('d M Y'),
                            'Name' => $player->firstname.' '.$player->lastname,
                            'Birth Date' => Carbon::parse($player->dob)->format('d M Y'),
                            'Mobile' => $player->mobile,
                            'Aadhar Number' =>  $player->aadhar_number,
                            'Current Roll Number' => $player->player_education->current_roll_no,
                            'College Stream' => $player->player_education->college_stream,
                            'Eligibility Status' => $player->eligibility_status == 1 ? 'eligible' : 'Not Eligible',
                        ];
                    })->toArray();
                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.player.view',[
            'players' => $players->orderBy('id','desc')->paginate(50)
        ]);
    }

    public function edit(Request $request)
    {
        
        if($request->isMethod('post'))
        {

            $this->validate($request,[
                'firstname' => 'required',
                'father_name' => 'required',
                'lastname' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:players,mobile,'.$request->id,
                'email' => 'email|unique:players,email,'.$request->id,
                'password' => 'required|min:6',
                'aadhar_number' => 'digits:12|unique:players,aadhar_number,'.$request->id,
                'eligibility_status' => 'required',
                'ssc_pass_year' => 'date',
                'hsc_pass_year' => 'date',
                'college_started_year' => 'date',
                'current_college_year' => 'digits:1',
                'total_college_year' => 'digits:1',
            ], [
                'firstname.required' => 'First Name is Required',
                'father_name.required' => 'Father Name is Required',
                'lastname.required' => 'Last Name is Required',
                'gender.required' => 'Gender is Required',
                'dob.required' => 'Date of Birth is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.exists' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.email' => 'Invalid Email Address',
                'email.exists' => 'This Email Address is already exist',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'aadhar_number.digits' => 'Aadhar Number Must be 12 digit number',
                'aadhar_number.exists' => 'This Aadhar Number is already exist',
                'eligibility_status.required' => 'Eligibility Status is Required',
                'ssc_pass_year.date' => 'SSC Pass Year must be a date',
                'hsc_pass_year.date' => 'HSC Pass Year must be a date',
                'college_started_year.date' => 'College Started Year must be a date',
                'current_college_year.digits' => 'Current College Year must be 1 digits',
                'total_college_year.digits' => 'Total College Year must be 1 digits',
            ]);

            $player = Player::find($request->id);
            $player->firstname         = $request->firstname;
            $player->father_name       = $request->father_name;
            $player->lastname          = $request->lastname;
            $player->gender             = $request->gender;
            $player->dob                = Carbon::parse($request->dob)->format('Y-m-d');
            $player->mobile             = $request->mobile;
            $player->email              = $request->email;
            $player->password           = $request->password;
            $player->aadhar_number      = $request->aadhar_number;
            $player->eligibility_status = $request->eligibility_status;
            $player->save();

            $player_education = PlayerEducation::wherePlayerId($request->id)->first();
            $player_education->hsc_pass_year = $request->hsc_pass_year;
            $player_education->college_started_year = $request->college_started_year;
            $player_education->current_college_year = $request->current_college_year;
            $player_education->total_college_year = $request->total_college_year;
            $player_education->current_roll_no = $request->current_roll_no;
            $player_education->college_stream = $request->college_stream;
            $player_education->save();

            return redirect()->back()->with(['success' => 'Player Details has been updated']);
        }


        $player = Player::with('player_education')->whereId($request->id)->first();
        return view('admin.player.edit', [
            'player' => $player,
        ]);
    }

    // public function view_account($id)
    // {
    //     if(!$user = User::whereId($id)->first())
    //         return redirect()->back()->with(['error' => 'Invalid User Details for View']);

    //     return redirect()->route('user-admin-view-access', ['id' => $user->id]);

    // }


}
