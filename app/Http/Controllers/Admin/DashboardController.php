<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Models\Game;
use App\Models\Player;
use App\Models\PlayerDocument;
use App\Models\GameParticipant;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard', [
            'players' => Player::count(),
            'games' => Game::count(),
            'participants' => GameParticipant::count(),
            'unverified_documents' => PlayerDocument::whereStatus(PlayerDocument::PENDING)->count() 
        ]);
    }

}
