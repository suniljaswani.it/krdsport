<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Excel;
use Carbon\Carbon;

// Models
use App\Models\PlayerDocument;
use App\Models\DocumentType;

class PlayerDocumentController extends Controller
{
    public function index(Request $request)
    {
        $documents = PlayerDocument::with(['player.player_education', 'documentType'])
            ->search($request->search, [
            'image', 'player.firstname', 'player.lastname', 'player.mobile', 'player.aadhar_number', 'player.player_education.current_roll_no', 'documentType.name_english'
        ]);

        if($request->document_type)
        {
            $documents->where('document_type_id', $request->document_type);
        }

        if($request->status)
        {
            $documents->whereStatus($request->status);
        }

        if($request->daterange)
        {
            $dates = explode('-', $request->daterange);
            $start_date = Carbon::parse(trim($dates[0]));
            $end_date = Carbon::parse(trim($dates[1]));

            $documents = $documents->where([
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $start_date == $end_date ? $end_date->addDay(1) : $end_date]
            ]);
        }

        if ($request->download == 'yes') {
            Excel::create('Player Documents', function ($excel) use ($documents, $request) {

                $excel->sheet('Sheet1', function ($sheet) use ($documents, $request) {

                    if (!empty($request->daterange) || !empty($request->search) || !empty($request->package)) {
                        $documents = $documents->orderBy('created_at', 'desc')->get();
                    } else {
                        $documents = $documents->orderBy('id', 'desc')->paginate(50);
                    }
                    $data = $documents->map(function ($document) {

                        return [
                            'Created Date' => Carbon::parse($document->created_at)->format('d M Y'),
                            'Player Name' => $document->player->firstname . ' ' .$document->player->father_name.' '.$document->player->lastname,
                            'Mobile' => $document->player->mobile,
                            'Document Type' => $document->documentType->name_english,
                            'Current Roll Number' => $document->player->player_education->current_roll_no,
                            'College Stream' => $document->player->player_education->college_stream,
                            'Status' => $document->status,
                            'For Status Info' => '1 => Pending, 2 => Verified, 3 => Rejected',
                        ];
                    })->toArray();
                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        $documentTypes = DocumentType::get();

        return view('admin.player-document.list',[
            'documents' => $documents->orderBy('id','desc')->paginate(50),
            'documentTypes' => $documentTypes
        ]);
    }

    public function update(Request $request)
    {
        $document = PlayerDocument::with(['player.player_education', 'documentType'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'status' => 'required'
            ],[
                'status.required' => 'Status is Required'
            ]);

            $document->status = $request->status;
            $document->remarks = $request->remarks;
            $document->save();

            return redirect()->route('admin-player-document-list')->with(['success' => 'Player Document has been Updated successfully..']);

        }

        return view('admin.player-document.update', ['document' => $document]);
    }


}
