<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

// --- Models --- //
use App\Models\Game;
use App\Models\GameType;


class GameController extends Controller
{
    public function index(Request $request)
    {
        $games = Game::with(['gameType'])
            ->search($request->search, [
                'name', 'gameType.name'
            ]);

        if ($request->type) {
            $games->where('game_type_id', $request->type);
        }

        if ($request->status) {
            $games->whereStatus($request->status);
        }

        if ($request->daterange) {
            $dates = explode('-', $request->daterange);
            $start_date = Carbon::parse(trim($dates[0]));
            $end_date = Carbon::parse(trim($dates[1]));

            $games = $games->where([
                ['start_date', '>=', $start_date],
                ['start_date', '<=', $start_date == $end_date ? $end_date->addDay(1) : $end_date]
            ]);
        }

        
        return view('admin.game.list', [
            'games' => $games->orderBy('id', 'desc')->paginate(20),
            'gameTypes' => GameType::get()
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'type' => 'required',
                'name' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'status' => 'required'
            ], [
                'type.required' => 'Game Type is Required',
                'name.required' => 'Name is Required',
                'start_date.required' => 'Start Date is Required',
                'end_date.required' => 'End Date is Required',
                'status.required' => 'Status is Required'
            ]);


            $game = Game::create([
                'game_type_id' => $request->type,
                'name' => $request->name,
                'start_date' => Carbon::parse($request->start_date)->format('Y-m-d'),
                'end_date' => Carbon::parse($request->end_date)->format('Y-m-d'),
                'status' => $request->status,
            ]);


            return redirect()->route('admin-game-list')->with(['success' => 'New Game has been created..!!']);
        }

        return view('admin.game.create',[
            'gameTypes' => GameType::get()
        ]);
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validate($request, [
                'type' => 'required',
                'name' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'status' => 'required'
            ], [
                'type.required' => 'Game Type is Required',
                'name.required' => 'Name is Required',
                'start_date.required' => 'Start Date is Required',
                'end_date.required' => 'End Date is Required',
                'status.required' => 'Status is Required'
            ]);

            $game = Game::find($request->id);
            $game->game_type_id = $request->type;
            $game->name         = $request->name;
            $game->start_date   = Carbon::parse($request->start_date)->format('Y-m-d');
            $game->end_date     = Carbon::parse($request->end_date)->format('Y-m-d');
            $game->status       = $request->status;
            $game->save();

            return redirect()->route('admin-game-list')->with(['success' => 'Game has been updated..!!']);
        }

        return view('admin.game.update', [
            'game' => Game::with(['gameType'])->whereId($request->id)->first(),
            'gameTypes' => GameType::get()
        ]);
    }

}
