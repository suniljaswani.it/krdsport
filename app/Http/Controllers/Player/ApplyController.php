<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Carbon\Carbon;

//--- Models ---//
use App\Models\Player;
use App\Models\Game;
use App\Models\GameType;
use App\Models\GameParticipant;


class ApplyController extends Controller
{
    public $player;

    public function __construct()
    {
        $this->player = Session::get('player');
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            
            $this->validate($request, [
                'type' => 'required',
            ],[
                'type.required' => 'Type is Required',
            ]);

            $participate = GameParticipant::create([
                'player_id' => $this->player->id, 
                'game_id' => $request->game_for_type_1 != NULL ? $request->game_for_type_1 : $request->game_for_type_2,
                'gender' => Session::get('player')->gender,
            ]);
            
            return redirect()->route('player-apply-list')->with(['success' => 'Player has been successfully applied for the game.']);

        }

        $gameTypes = GameType::active()->get();
        $gameOnes = Game::where('game_type_id', 1)->active()->get();
        $gameTwos = Game::where('game_type_id', 2)->active()->get();
        return view('player.apply.create', [
            'gameTypes' => $gameTypes,
            'gameOnes' => $gameOnes,
            'gameTwos' => $gameTwos,
            'oneTypeName' => GameType::whereId(1)->first(),
            'twoTypeName' => GameType::whereId(2)->first(),
        ]);
    }

    public function index(Request $request)
    {
        $participants = GameParticipant::with(['game.gameType'])->wherePlayerId($this->player->id);   
        return view('player.apply.list',[
            'participants' => $participants->orderBy('id', 'desc')->paginate(10),
        ]);
    }

    public function edit(Request $request)
    {
        if($request->isMethod('post'))
        {
            $apply = GameParticipant::find($request->id);
            $apply->game_id = $request->game_id;
            $apply->save();
            
            return redirect()->route('player-apply-list')->with(['success' => 'Applied Game has been successfully Updated']);
        }

    

        $participate_info = GameParticipant::with(['game.gameType'])->whereId($request->id)->first();
        $games = Game::where('game_type_id', $participate_info->game->gameType->id)->active()->get();
        return view('player.apply.edit',[
            'participate_info' => $participate_info,
            'games' => $games,
        ]);
    }

}
