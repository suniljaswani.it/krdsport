<?php

namespace App\Http\Controllers\Player;

// --- Models --- //
use App\Models\Player;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;


class DashboardController extends Controller
{
    public $player;

    public function __construct()
    {
        $this->player = Session::get('player');
    }

    public function index(Request $request)
    {
        return view('player.dashboard', [
            'player' => Player::whereId($this->player->id)->first(),
        ]);
    }

    

    
}
