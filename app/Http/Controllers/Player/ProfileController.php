<?php

namespace App\Http\Controllers\Player;

// use Image;
// use Storage;
// use Uuid;
use Session;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Models
use App\Models\Player;
use App\Models\PlayerEducation;

use Carbon\Carbon;

class ProfileController extends Controller
{
    public $player;

    public function __construct()
    {
        $this->player = Session::get('player');
    }

    public function index(Request $request)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'firstname' => 'required',
                'father_name' => 'required',
                'lastname' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                // 'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:players,mobile,' .Session::get('player')->id,
                'email' => 'email|unique:players,email,' .Session::get('player')->id,
                'aadhar_number' => 'digits:12|unique:players,aadhar_number,' .Session::get('player')->id,
                'hsc_pass_year' => 'date',
                'ssc_pass_year' => 'date',
                'college_started_year' => 'date',
                'current_college_year' => 'digits:1',
                'total_college_year' => 'digits:1',
            ], [
                'firstname.required' => 'First Name is Required',
                'father_name.required' => 'Father Name is Required',
                'lastname.required' => 'Last Name is Required',
                'gender.required' => 'Gender is Required',
                'dob.required' => 'Date of Birth is Required',
                // 'mobile.required' => 'Mobile Number is Required',
                // 'mobile.exists' => 'This Mobile Number is already exist',
                // 'mobile.regex' => 'Invalid Mobile Number',
                // 'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.email' => 'Invalid Email Address',
                'email.exists' => 'This Email Address is already exist',
                'aadhar_number.digits' => 'Aadhar Number Must be 12 digit number',
                'aadhar_number.exists' => 'This Aadhar Number is already exist',
                'ssc_pass_year.date' => 'SSC Pass Year must be a Date',
                'hsc_pass_year.date' => 'HSC Pass Year must be a Date',
                'college_started_year.digits' => 'College Started Year must be a Date',
                'current_college_year.digits' => 'Current College Year must be 1 digits',
                'total_college_year.digits' => 'Total College Year must be 1 digits',
            ]);

            $player = Player::find(Session::get('player')->id);
            $player->firstname = $request->firstname;
            $player->father_name = $request->father_name;
            $player->lastname = $request->lastname;
            $player->gender = $request->gender;
            $player->dob = Carbon::parse($request->dob)->format('Y-m-d');
            // $player->mobile = $request->mobile;
            $player->email = $request->email;
            $player->aadhar_number = $request->aadhar_number;
            $player->save();
            
            

            $player_education = PlayerEducation::wherePlayerId(Session::get('player')->id)->first();
            if($player_education)
            {
                $player_education->ssc_pass_year = Carbon::parse($request->ssc_pass_year)->format('Y-m-d');
                $player_education->hsc_pass_year = Carbon::parse($request->hsc_pass_year)->format('Y-m-d');
                $player_education->college_started_year = Carbon::parse($request->college_started_year)->format('Y-m-d');
                $player_education->current_college_year = $request->current_college_year;
                $player_education->total_college_year = $request->total_college_year;
                $player_education->current_roll_no = $request->current_roll_no;
                $player_education->college_stream = $request->college_stream;
                $player_education->save();
            }
            else
            {
                $player_education = PlayerEducation::create([
                    'player_id' => Session::get('player')->id,
                    'ssc_pass_year' => Carbon::parse($request->ssc_pass_year)->format('Y-m-d'),
                    'hsc_pass_year' => Carbon::parse($request->hsc_pass_year)->format('Y-m-d'),
                    'college_started_year' => Carbon::parse($request->college_started_year)->format('Y-m-d'),
                    'current_college_year' => $request->current_college_year,
                    'total_college_year' => $request->total_college_year,
                    'current_roll_no' => $request->current_roll_no,
                    'college_stream' => $request->college_stream,
                ]);
            }


            // if($request->hasFile('profile_image') && $request->file('profile_image')->isValid())
            // {
            //     $s3 = Storage::disk('s3');
            //     $profile_image = Uuid::generate(4).$request->file('profile_image')->getClientOriginalName();
            //     $s3->delete(env('PROFILE_IMAGE_PATH').$user_detail->image);
            //     $s3->put(env('PROFILE_IMAGE_PATH').$profile_image, file_get_contents($request->file('profile_image')->path()));
            //     $user_detail->image = $profile_image;
            // }

            return redirect()->route('player-dashboard')->with(['success' => 'Your Profile has been Updated!!..']);
        }

        $player = Player::with('player_education')->whereId(Session::get('player')->id)->first();
        return view('player.profile',[
            'player' => $player
        ]);

    }

    public function password(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'old_password' => 'required|min:6|exists:players,password,id,'.$this->player->id,
                'new_password' => 'required|min:6'
            ], [
                'old_password.required' => 'Old Password is required',
                'old_password.min' => 'Old Password length should be 6 or more',
                'old_password.exists' => 'Invalid Old Password',
                'new_password.required' => 'New Password is required',
                'new_password.min' => 'New Password length should be 6 or more'
            ]);

            $player = Player::find($this->player->id);
            $player->password = $request->new_password;
            $player->save();

            return redirect()->back()->with(['success' => 'Player Password has been Updated.']);
        }
        return view('player.security.password');
    }

}
