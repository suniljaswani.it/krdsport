<?php

namespace App\Http\Controllers\Player;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;

// Models
use App\Models\Player;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post')){

            $this->validate($request, [
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10',
                'password' => 'required'

            ], [
                'mobile.required' => 'Mobile Number is Required',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'password.required' => 'Password is Required'
            ]);

            if($checkMobile = Player::where('mobile', $request->mobile)->first())
            {
                if($request->password == $checkMobile->password){
                    Session::put('player', $checkMobile);
                }
                return redirect()->back()->with('error', 'Password is Not Match with Mobile!');
            }
            return redirect()->back()->with('error', 'Invalid Mobile...!!');
        }

        if(Session::has('player'))
            return redirect()->route('player-dashboard');
        return view('player.login');
    }

    public function logout()
    {
        Session::forget('player');
        // Session::forget('secure_user');
        return redirect()->route('player-login');
    }

    // public function adminViewAccess($id)
    // {
    //     if (!Session::has('admin'))
    //         return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

    //     if (!$user = User::whereId($id)->first())
    //         return redirect()->back()->with(['error' => 'Invalid Credentials for User Access']);

    //     Session::put('user', $user);
    //     return redirect()->route('associate-dashboard');

    // }
}
