<?php

namespace App\Http\Controllers\Player;

use Image;
use Storage;
use Session;
use Uuid;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Models
use App\Models\Player;
use App\Models\PlayerDocument;
use App\Models\DocumentType;

class PlayerDocumentController extends Controller
{
    public $player;

    public function __construct()
    {
        $this->player =  Session::get('player');
    }

    public function index(Request $request)
    {
        $documents = PlayerDocument::with('player', 'documentType')->wherePlayerId($this->player->id);
        $documentTypes = DocumentType::get();

        if($request->type)
        {
            $documents->whereDocumentTypeId($request->type);
        }

        if($request->status)
        {
            $documents->whereStatus($request->status);
        }

        return view('player.player-document.list', [
            'documents' => $documents->latest()->paginate(20),
            'documentTypes' => $documentTypes
        ]);
    }

    public function upload(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
               'type' => 'required',
               'image' => 'required|mimes:jpeg,jpg,png|max:2000',
            ],[
                'type.required' => 'Type is Required',
                'image.image' => 'Invalid image',
                'image.max' => 'Image Size should be less than 2MB',
                'image.required' => 'Upload File is Required',
            ]);
            
            // $path = public_path('player-assets/images/player-documents/'.$this->player->id . '/');
            // dd($path);

            if($request->hasFile('image') && $request->file('image')->isValid())
            {
                
                $file = $request->file('image');

                $fileName = Uuid::generate(4).$file->getClientOriginalName();
                // $destinationPath = env('PLAYER_DOCUMENT_PATH').$this->player->id.'/';
                $request->image->move(public_path('player-assets/images/player-documents/' . $this->player->id . '/'), $fileName);
                
                
                $document = PlayerDocument::create([
                    'player_id' => $this->player->id,
                    'image' => $fileName,
                    'document_type_id' => $request->type,
                ]);
            }

            return redirect()->route('player-document-list')->with(['success' => 'Document Uploded Successfully..']);
        }

        return view('player.player-document.upload', [
            'documentTypes' => DocumentType::get()
        ]);
    }


}
