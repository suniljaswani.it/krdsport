<?php

namespace App\Http\Controllers\Associate;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;

// Models
use App\Models\Support;

class SupportController extends Controller
{

	public function index()
	{
		$supports = Support::orderBy('id','desc')->whereUserId(Session::get('user')->id);

		return view('associate.quick-support',[
			'supports' => $supports->paginate(20)
		]);

	}

	public function create(Request $request)
	{
		if($request->isMethod('post'))
    	{
    		$this->validate($request,[
    			'category' => 'required',
    			'subject' => 'required',
    			'message' => 'required'
    		],[
    			'category.required' => 'Category is Required',
    			'subject.required' => 'Subject is Required',
    			'message.required' => 'Message is Required'
    		]);

    		$support = Support::create([
    			'category' => $request->category,
    			'subject' => $request->subject,
    			'message' => $request->message,
    			'user_id' => Session::get('user')->id
    		]);

    		return redirect()->route('associate-quick-support')->with(['success' => 'Quick Support request has been Created..!!']);
    	}

    	$support_categories = \File::get(public_path('data/support_categories.json'));
    	return view('associate.quick-support-create',[
    		'support_categories' => json_decode($support_categories),
    	]);

	}

}
