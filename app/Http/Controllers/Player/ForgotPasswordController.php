<?php

namespace App\Http\Controllers\Associate;

use Mail;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
               'email' => 'required|email|exists:users,email'
            ],[
                'email.required' => 'Email is Required',
                'email.email' => 'Invalid Email',
                'email.exists' => 'Wrong Email',
            ]);

            $user = User::with('user_detail')->where('email', $request->email)->first();
            $user->remember_token = str_random(100);
            $user->save();


            Mail::send('associate.email.forgot', ['user' => $user], function ($mail) use ($user) {
                $mail->from('seyagoindia1@gmail.com', 'Seyago India Support');
                $mail->to($user->email, $user->user_detail->full_name)->subject('Forgot Password Link From Seyago India');
            });

            return redirect()->route('associate-forgot-password')->with(['success' => 'Kindly check your mail and proceed.']);

        }
        return view('associate.forgot-password');
    }

    public function reset(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'new_password' => 'required|min:6',
                're_type_password' => 'required|min:6|same:new_password',
            ],[
                'new_password.required' => 'New Password is required',
                'new_password.min' => 'New Password length should be 6 or more',
                're_type_password.required' => 'Re-Type Password is required',
                're_type_password.min' => 'Re-Type Password length should be 6 or more',
                're_type_password.same' => 'Password and Re-Type Password Must be a same ',
            ]);

            $user = User::whereId($request->id)->where('remember_token', $request->key)->first();

            if(!$user)
                return redirect()->back()->with(['error' => 'Invalid Token']);

            $user->password = $request->new_password;
            $user->save();

            return redirect()->route('associate-login')->with(['success' => 'New Password has been Created Successfully.. Kindly Login with New Password']);
        }
        return view('associate.reset-password', ['id' => '', 'key' => '']);
    }
}
