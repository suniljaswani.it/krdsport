<?php 
/*
|--------------------------------------------------------------------------
| Player Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Player', 'prefix' => 'player'], function () 
{
    
    Route::get('/', 'LoginController@index')->name('player-login');
    Route::post('/', 'LoginController@index')->name('player-login');
    Route::get('/logout', 'LoginController@logout')->name('player-logout');


    Route::get('forgot-password', 'ForgotPasswordController@index')->name('player-forgot-password');
    Route::post('forgot-password', 'ForgotPasswordController@index')->name('player-forgot-password');

    Route::get('reset-password/{id}/{key}', 'ForgotPasswordController@reset')->name('player-reset-password');
    Route::post('reset-password/{id}/{key}', 'ForgotPasswordController@reset')->name('player-reset-password');


    // Route::get('register-step-1', 'RegisterController@step1')->name('website-register-step-1');
    // Route::post('register-step-1', 'RegisterController@step1')->name('website-register-step-1');
    // Route::get('register-step-2', 'RegisterController@step2')->name('website-register-step-2');
    // Route::post('register-step-2', 'RegisterController@step2')->name('website-register-step-2');
    // Route::get('register-step-3', 'RegisterController@step3')->name('website-register-step-3');
    // Route::post('register-step-3', 'RegisterController@step3')->name('website-register-step-3');

    Route::group(['middleware' => 'playerAuth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('player-dashboard');

        Route::group(['middleware' => 'playerEligibility'], function () {
            
            Route::group(['prefix' => 'apply'], function () {
                
                Route::get('create', 'ApplyController@create')->name('player-apply-create');
                Route::post('create', 'ApplyController@create')->name('player-apply-create');
                Route::get('edit/{id}', 'ApplyController@edit')->name('player-apply-edit');
                Route::post('edit/{id}', 'ApplyController@edit')->name('player-apply-edit');
                Route::get('list', 'ApplyController@index')->name('player-apply-list');
    
            });

        });
        
        Route::get('profile', 'ProfileController@index')->name('player-profile');
        Route::post('profile', 'ProfileController@index')->name('player-profile');

        Route::get('password', 'ProfileController@password')->name('player-profile-password');
        Route::post('password', 'ProfileController@password')->name('player-profile-password');

        // Route::get('quick-support', 'SupportController@index')->name('player-quick-support');
        // Route::get('quick-support/create', 'SupportController@create')->name('player-quick-support-create');
        // Route::post('quick-support/create', 'SupportController@create')->name('player-quick-support-create');

        Route::group(['prefix' => 'player-document'], function () {

            Route::get('/list', 'PlayerDocumentController@index')->name('player-document-list');
            Route::get('/upload', 'PlayerDocumentController@upload')->name('player-document-upload');
            Route::post('/upload', 'PlayerDocumentController@upload')->name('player-document-upload');

        });

        
        Route::group(['prefix' => 'games'], function () {

            Route::get('/participate-list', 'ParticipateController@index')->name('player-game-participate-list');
            Route::get('/participate', 'ParticipateController@create')->name('player-game-participate');
            Route::post('/participate', 'ParticipateController@create')->name('player-game-participate');

        });



    });


});



?>