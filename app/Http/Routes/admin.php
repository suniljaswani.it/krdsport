<?php 
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
{

    Route::get('/', 'LoginController@index')->name('admin-login');
    Route::post('/', 'LoginController@index')->name('admin-login');
    Route::get('/logout', 'LoginController@logout')->name('admin-logout');

    Route::group(['middleware' => 'adminAuth'], function()
    {
       
        Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');

        Route::get('access-denied', function () {
            return view('admin.access-denied');
        })->name('admin-access-denied');

        Route::get('profile', 'AdminController@profile')->name('admin-profile');
        Route::post('profile', 'AdminController@profile')->name('admin-profile');

        Route::group(['prefix' => 'manager'], function () {
            Route::get('/', 'AdminController@index')->name('admin-manager-view');
            Route::get('/create', 'AdminController@create')->name('admin-manager-create');
            Route::post('/create', 'AdminController@create')->name('admin-manager-create');
            Route::get('/update/{id}', 'AdminController@update')->name('admin-manager-update');
            Route::post('/update/{id}', 'AdminController@update')->name('admin-manager-update');
        });

        Route::group(['prefix' => 'player'], function () {
            Route::get('/', 'PlayerController@index')->name('admin-player-detail');
            Route::post('/', 'PlayerController@index')->name('admin-player-detail');
            
            Route::get('edit/{id}', 'PlayerController@edit')->name('admin-player-edit');
            Route::post('edit/{id}', 'PlayerController@edit')->name('admin-player-edit');
        
        });

        Route::group(['prefix' => 'apply'], function () {
            Route::get('/', 'ApplyController@index')->name('admin-apply-list');
            Route::post('/', 'ApplyController@index')->name('admin-apply-list');

            Route::get('edit/{id}', 'ApplyController@edit')->name('admin-apply-edit');
            Route::post('edit/{id}', 'ApplyController@edit')->name('admin-apply-edit');

        });

        Route::group(['prefix' => 'player-document'], function () {
            Route::get('/list', 'PlayerDocumentController@index')->name('admin-player-document-list');
            Route::get('/list/{id}', 'PlayerDocumentController@update')->name('admin-player-document-update');
            Route::post('/list/{id}', 'PlayerDocumentController@update')->name('admin-player-document-update');
        });

        
        Route::group(['prefix' => 'game'], function () {
            Route::get('/list', 'GameController@index')->name('admin-game-list');
            Route::get('/create', 'GameController@create')->name('admin-game-create');
            Route::post('/create', 'GameController@create')->name('admin-game-create');
            Route::get('/list/{id}', 'GameController@update')->name('admin-game-update');
            Route::post('/list/{id}', 'GameController@update')->name('admin-game-update');
        });



    });


});





?>