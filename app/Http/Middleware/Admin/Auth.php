<?php

namespace App\Http\Middleware\Admin;

use Session;
use App\Models\AdminRole;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('admin'))
            return redirect()->route('admin-login')->with(['error' => 'Session has been Expire, Login Again']);

        $current_route = \Route::getCurrentRoute()->getName();

        if(!in_array($current_route, AdminRole::openRoutes())){
            if(!AdminRole::where('route', $current_route)->where('admin_id', Session::get('admin')->id )->first())
                return redirect()->route('admin-access-denied');
        }
        return $next($request);
    }
}
