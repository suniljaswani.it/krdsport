<?php

namespace App\Http\Middleware\Player;

use Session;
use Closure;

class Security
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_route = \Route::getCurrentRoute()->getName();

        if(!Session::has('secure_user'))
            return redirect()->route('player-secure-login', ['page' => $current_route])->with(['error' => 'Session has been Expire, Login Again']);
        return $next($request);
    }
}
