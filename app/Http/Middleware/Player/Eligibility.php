<?php

namespace App\Http\Middleware\Player;

use Session;
use Closure;

//--- Models ---//
use App\Models\Player;
use App\Models\PlayerEducation;

class Eligibility
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $eligibility = PlayerEducation::checkEligibility();
        if($eligibility == true)
        {
            return $next($request);
        }
        else
        {
            return redirect()->route('player-dashboard')->with(['error' => 'You are not eligible to apply, Kindly update your education details to Eligible!']);
        }
    }
}
