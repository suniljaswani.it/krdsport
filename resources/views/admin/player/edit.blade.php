@extends('admin.template.layout')

@section('title', 'Edit Player Details')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-title-box">
			<h4 class="page-title">Edit Player Details</h4>
			<ol class="breadcrumb p-0 m-0">
				<li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
				<li> <a href="{{ route('admin-player-detail') }}">Players</a> </li>
				<li class="active"> Edit Player Details </li>
			</ol>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row text-center">
    <div class="col-md-6 col-md-offset-3">
    @if (session('errors'))
        <div class="alert alert-danger">
            <ul>
                @foreach (session('errors')->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
    @endif
    </div>
</div>
<div class="row">
	<form action="" method="post" role="form" enctype="multipart/form-data">
	<div class="col-md-6">
		<div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Information</h3>
            </div>
            <div class="panel-body">
				{{ csrf_field() }}
                <div class="row form-group">
                    <label>First Name</label>
                    <input type="text" name="firstname" class="form-control" value="{{ $player->firstname }}" required>
                </div>
                <div class="row form-group">
                    <label>Father Name</label>
                    <input type="text" name="father_name" class="form-control" value="{{ $player->father_name }}" required>
                </div>
                <div class="row form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastname" class="form-control" value="{{ $player->lastname }}" required>
                </div>
                <div class="row form-group">
                    <label>Gender</label>
                    <div class="radio">
                        <input type="radio" name="gender" value="1" {{ $player->gender == 1 ? 'checked' : '' }}>
                        <label>Male</label>
                    </div>
                    <div class="radio">
                        <input type="radio" name="gender" value="2" {{ $player->gender == 2 ? 'checked' : '' }}>
                        <label>Female</label>
                    </div>
                </div>
                <div class="row form-group">
                    <label>Date of Birth</label>
                    <div class="input-group">
                        <input type="text" name="dob" value="{{ Carbon\Carbon::parse($player->dob)->format('d-m-Y') }}" class="form-control" placeholder="dd-mm-yyyy" id="datepicker-autoclose1" required>
                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label>Aadhar Number</label>
                    <input type="text" name="aadhar_number" class="form-control" value="{{ $player->aadhar_number }}" placeholder="Enter Your Aadhar Number">
                </div>
                <div class="row form-group">
                    <label>Mobile Number</label>
                    <input type="number" name="mobile" min="1" class="form-control" value="{{ $player->mobile }}" placeholder="Enter Your Mobile Number" required>
                </div>
                <div class="row form-group">
                    <label>Email ID</label>
                    <input type="email" name="email" class="form-control" value="{{ $player->email }}" >
                </div>
                <div class="row form-group">
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" value="{{ $player->password }}" required>
                </div>
                <div class="row form-group">
                    <label>Eligibility Status</label>
                    <select name="eligibility_status" class="form-control" required>
                        <option value="1" {{ $player->eligibility_status == '1' ? 'selected' : 'Eligible' }}>Eligible</option>
                        <option value="2" {{ $player->eligibility_status == '2' ? 'selected' : 'Not Eligible' }}>Not Eligible</option>
                    </select>
                </div>
            </div>
        </div>
	</div>
    <div class="col-md-6">
        <div class="panel panel-border panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Education Information</h3>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <label>SSC Pass Year</label>
                    <div class="input-group">
                        <input type="text" name="ssc_pass_year" value="{{ $player->player_education->ssc_pass_year }}" class="form-control" placeholder="yyyy" id="datepicker-autoclose4" required>
                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label>HSC Pass Year</label>
                    <div class="input-group">
                        <input type="text" name="hsc_pass_year" value="{{ $player->player_education->hsc_pass_year }}" class="form-control" placeholder="yyyy" id="datepicker-autoclose2" required>
                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label>College Started Year</label>
                    <div class="input-group">
                        <input type="text" name="college_started_year" value="{{ $player->player_education->college_started_year }}" class="form-control" placeholder="yyyy" id="datepicker-autoclose2" required>
                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label>Current College Year</label>
                    <select name="current_college_year" class="form-control" >
                        @for ($i = 1; $i <= 5; $i++)
                            <option value="{{ $i }}" {{ $player->player_education->current_college_year == $i ? 'selected' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="row form-group">
                    <label>Total College Year</label>
                    <select name="total_college_year" class="form-control" >
                        @for ($j = 3; $j <= 5; $j++)
                            <option value="{{ $j }}" {{ $player->player_education->total_college_year == $j ? 'selected' : '' }}>{{ $j }}</option>
                        @endfor
                    </select>
                </div>
                <div class="row form-group">
                    <label>Current Roll Number</label>
                    <input type="text" name="current_roll_no" class="form-control" value="{{ $player->player_education->current_roll_no }}" >
                </div>
                <div class="row form-group">
                    <label>College Stream</label>
                    <input type="text" name="college_stream" class="form-control" value="{{ $player->player_education->college_stream }}" >
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-danger waves-effect w-md waves-light m-b-5">Update</button>
    </div>
	</form>
</div>

@stop

@section('import-javascript')
	<script src="/admin-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/admin-assets/plugins/switchery/switchery.min.js"></script>
@endsection

@section('page-javascript')
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$('#datepicker-autoclose1').datepicker({
				autoclose: true,
	            format: "dd-mm-yyyy",
	            startDate: "-99y",
	            endDate: "-18y"
		    });
            $('#datepicker-autoclose2, #datepicker-autoclose3, #datepicker-autoclose4').datepicker({
                autoclose: true,
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years"
            });
        });
	</script>
@endsection

@section('import-css')
    <link href="/admin-assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/admin-assets/plugins/switchery/switchery.min.css">
@endsection
