@extends('admin.template.layout')

@section('title', 'Players Detail')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Players Detail</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
                    <li> <a href="javascript:void(0)">Players</a> </li>
                    <li class="active"> Players Detail </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>

    <div class="row m-b-10">
        <div class="col-md-6 text-left">
            &nbsp;
        </div>
        <div class="col-md-6 text-right">
            <a href="{{ route('admin-player-detail') }}" class="btn btn-purple btn-sm m-r-10" title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="{{ route('admin-player-detail', ['download' => 'yes', 'eligibility_status' => Request::get('eligibility_status'), 'search' => Request::get('search'), 'daterange' => Request::get('daterange')]) }}" class="btn btn-success btn-sm" title="Download">
                <i class="fa fa-download"></i>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="card-box">
            <div class="row m-b-20">
                <form action="" method="get">
                    <div class="col-md-3 m-t-10">
                        <div class="input-group" style="width: 100%">
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                            <input type="text" name="search" class="form-control" placeholder="Search Keyword" autocomplete="off" value="{{ Request::get('search') }}">
                        </div>
                    </div>
                    <div class="col-md-3 m-t-10">
                        <select name="eligibility_status" class="form-control">
                            <option value="">Select Eligibility Status</option>
                                <option value="1" {{ Request::get('eligibility_status') == 1 ? 'selected' : '' }}> Eligible </option>
                                <option value="2" {{ Request::get('eligibility_status') == 2 ? 'selected' : '' }}> Not Eligible </option>
                        </select>
                    </div>
                    <div class="col-md-4 m-t-10">
                        <div class="input-group">
                        <span class="input-group-btn">
						<button type="button" class="btn waves-effect waves-light btn-brown"><i class="fa fa-calendar"></i></button>
					</span>
                            <input class="form-control date-range" type="text" name="daterange" placeholder="Date Range for Joining Date" readonly>
                        </div>
                    </div>
                    <div class="col-md-1 m-t-10">
                        <button class="btn btn-danger"> Search </button>
                    </div>
                </form>
            </div>
            <div class="table-responsive" id="PlayerDetails">
                <table class="table table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th></th>
                        <th> Actions </th>
                        <th> Joining Date </th>
                        <th> Name </th>
                        <th> D.O.B. </th>
                        <th> Mobile </th>
                        <th> Aadhar No. </th>
                        <th> Eligibility Status </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($players as $key => $player)
                        <tr id="player{{ $key+1 }}" class="accordion-toggle" data-toggle="collapse" data-parent="#PlayerDetails" data-target=".playerDetails{{ $key+1 }}">
                            <td>
                                <i class="indicator glyphicon glyphicon-chevron-down pull-left">
                            </td>
                            <td>
                                <a href="{{ route('admin-player-edit', ['id' => $player->id]) }}" target="_blank"><button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-info"><i class="mdi mdi-pencil"></i></button></a>
                            </td>
                            <td>{{ \Carbon\Carbon::parse($player->created_at)->format('d M Y') }} </td>
                            <td>{{ $player->firstname.' '.$player->father_name.' '.$player->lastname }}</td>
                            <td>{{ \Carbon\Carbon::parse($player->dob)->format('d M Y') }} </td>
                            <td>{{ $player->mobile }}</td>
                            <td>{{ $player->aadhar_number }}</td>
                            <td>
                                @if ($player->eligibility_status == 1 )
                                    <button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="mdi mdi-check"></i></button>
                                @else
                                    <button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-danger"><i class="mdi mdi-close"></i></button>
                                @endif
                            </td>
                        </tr>

                        <tr id="playerDetails{{ $key+1 }}" class="hidden-row">
                            <td colspan="10" class="hiddenRow">
                                <div class="accordion-body collapse playerDetails{{ $key+1 }}" id="accordion{{ $key+1 }}">
                                    <div class="table-responsive">
                                        <table class="table table-bordered ">
                                            <thead>
                                            <tr>
                                                <th> Password </th>
                                                <th> Current Roll Number </th>
                                                <th> College Stream </th>
                                                <th> SSC Pass Year </th>
                                                <th> HSC Pass Year </th>
                                                <th> College Start </th>
                                                <th> Current College Year </th>
                                                <th> Total College Year </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{ $player->password }}</td>
                                                <td>{{ $player->player_education->current_roll_no }}</td>
                                                <td>{{ $player->player_education->college_stream }}</td>
                                                <td>{{ $player->player_education->ssc_pass_year }}</td>
                                                <td>{{ $player->player_education->hsc_pass_year }}</td>
                                                <td>{{ $player->player_education->college_started_year }}</td>
                                                <td>{{ $player->player_education->current_college_year }}</td>
                                                <td>{{ $player->player_education->total_college_year }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $players->appends(['search' => Request::get('search'), 'eligibility_status' => Request::get('eligibility_status'), 'daterange' => Request::get('daterange') ])->links() }}
        </div>
    </div>


@stop

@section('page-javascript')
    <script type="text/javascript">
        @foreach ($players as $key => $player)
        $('#accordion{{ $key+1 }}').on('shown.bs.collapse', function () {
            $("#player{{ $key+1 }} i.indicator").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $("#playerDetails{{ $key+1 }}").removeClass("hidden-row").addClass("show-row");
        });
        $('#accordion{{ $key+1 }}').on('hidden.bs.collapse', function () {
            $("#player{{ $key+1 }} i.indicator").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $("#playerDetails{{ $key+1 }}").removeClass("show-row").addClass("hidden-row");
        });
        @endforeach
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.date-range').daterangepicker({
                locale: {
                    format: 'DD MMM YYYY'
                },
                autoUpdateInput: false,
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-danger',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 15 Days': [moment().subtract(14, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });

        // $('#activate_user').click( function (e) {
        //     e.preventDefault();
        //     var self = $(this);
        //     var user_id = self.attr('data-user-id');

        //     swal({
        //             title: "Are you sure?",
        //             text: "After Activation, PV will add to All Uplines",
        //             type: "info",
        //             showCancelButton: true,
        //             confirmButtonColor: "#DD6B55",
        //             confirmButtonText: "Yes, I want it!",
        //             closeOnConfirm: false
        //         },
        //         function(){
        //             self.attr('disabled', true);
        //             swal('Processing...', 'Pv transfer process is running', 'info');
        //             $('form#activate_form'+user_id).submit();
        //         });
        // });

        // $('#transfer').click( function (e) {
        //     e.preventDefault();
        //     var self = $(this);
        //     var user_id = self.attr('data-user-id');

        //     swal({
        //             title: "Are you sure?",
        //             text: "After Transfer, Sponsor Income will add to Sponsor Upline",
        //             type: "info",
        //             showCancelButton: true,
        //             confirmButtonColor: "#DD6B55",
        //             confirmButtonText: "Yes, I want it!",
        //             closeOnConfirm: false
        //         },
        //         function(){
        //             self.attr('disabled', true);
        //             swal('Processing...', ' Sponsor Income has been transferred', 'info');
        //             $('form#transfer_form'+user_id).submit();
        //         });
        // });
    </script>
@stop


@section('import-javascript')
    <script src="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/admin-assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="/admin-assets/plugins/custombox/js/legacy.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
    <link href="/admin-assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
@stop

@section('page-css')
    <style type="text/css">
        .custom-table{
            font-size: 12px;
        }
        .custom-table > tbody > tr > td, .custom-table > thead > tr > th {
            padding: 5px 10px;
        }
        .sweet-alert{
            z-index: 10009;
        }
        .sweet-overlay {
            z-index: 10008;
        }
    </style>
@endsection
