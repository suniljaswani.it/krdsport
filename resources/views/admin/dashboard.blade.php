@extends('admin.template.layout')

@section('title', 'Dashboard')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li class="active"> Dashboard </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>

    {{-- Boxes --}}
    <div class="row">
        <div class="col-md-3">
        <a href="{{ route('admin-player-detail') }}">
            <div class="card-box widget-box-two widget-two-primary">
                <i class="fa fa-users widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Players</p>
                    <h2><span data-plugin="counterup">{{ $players }}</span> <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                    <p class="text-muted m-0">View More</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('admin-game-list') }}">
            <div class="card-box widget-box-two widget-two-warning">
                <i class="fa fa-gamepad widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Games </p>
                    <h2><span data-plugin="counterup">{{ $games }}</span> <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                    <p class="text-muted m-0">View More</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('admin-player-document-list') }}">
            <div class="card-box widget-box-two widget-two-danger">
                <i class="fa fa-file-pdf-o widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Unverified Doc</p>
                    <h2><span data-plugin="counterup">{{ $unverified_documents }}</span> <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                    <p class="text-muted m-0"> View More</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{ route('admin-apply-list') }}">
            <div class="card-box widget-box-two widget-two-success">
                <i class="mdi mdi-wunderlist widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Participants List</p>
                    <h2><span data-plugin="counterup">{{ $participants }}</span> <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                    <p class="text-muted m-0">View More</p>
                </div>
            </div>
            </a>
        </div>

    </div>

@stop
