<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Navigation</li>

                <li>
                    <a href="{{ route('admin-dashboard') }}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-wunderlist"></i> <span> Participants </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-apply-list') }}">List</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-multiple-outline"></i> <span> Players </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-player-detail') }}">Detail</a></li>
                    </ul>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-pdf-o"></i><span> Player Documents </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-player-document-list') }}"> List </a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-gamepad"></i><span> Games </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-game-create') }}"> Add</a></li>
                        <li><a href="{{ route('admin-game-list') }}"> List</a></li>
                    </ul>
                </li>

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-package"></i><span> Package </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-package-create') }}"> Create</a></li>
                        <li><a href="{{ route('admin-package-view') }}"> View</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-barcode"></i><span> Pin </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-pin-request-list') }}"> Request List</a></li>
                        <li><a href="{{ route('admin-pin-create') }}"> Create</a></li>
                        <li><a href="{{ route('admin-pin-view') }}"> View</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-rupee"></i><span> Payout </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-wallet-status') }}"> Wallet Status</a></li>
                        <li><a href="{{ route('admin-wallet-report') }}"> Wallet Report</a></li>
                        <li><a href="{{ route('admin-wallet-credit-debit') }}"> Wallet Credit-Debit</a></li>
                        <li><a href="{{ route('admin-payout-make') }}"> Make Payout</a></li>
                        <li><a href="{{ route('admin-payout-report') }}"> Payout Report</a></li>
                    </ul>
                </li> --}}
                
                

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-comment-question-outline"></i><span> Support </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-quick-support') }}"> Associate </a></li>
                        <li><a href="{{ route('admin-website-support') }}"> Website </a></li>
                    </ul>
                </li> --}}

                {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-trophy"></i><span> Rewards </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="{{ route('admin-reward') }}"> List </a></li>--}}
                        {{--<li><a href="{{ route('admin-reward-create') }}"> Create </a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-question"></i><span> Faqs </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-faqs') }}"> List </a></li>
                        <li><a href="{{ route('admin-faqs-create') }}"> Create </a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-image"></i><span> Popup </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('admin-popup-update') }}"> Website Popup </a></li>
                    </ul>
                </li> --}}

                {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-newspaper-o"></i><span> News </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="{{ route('admin-news') }}"> List </a></li>--}}
                        {{--<li><a href="{{ route('admin-news-create') }}"> Create </a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bars"></i><span> Contents </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        @php $contents = App\Models\Content::active()->get(); @endphp
                        @foreach($contents as $content)
                            <li><a href="{{ route('admin-content-update', ['id' => $content->id]) }}"> {{ $content->name }} </a></li>
                        @endforeach

                    </ul>
                </li> --}}


                <li class="menu-title">Extra</li>

                <li>
                    <a href="{{ route('admin-manager-view') }}" class="waves-effect"><i class="mdi mdi-gift"></i><span> Admin Roles </span> </a>
                </li>


                {{-- <li class="menu-title">More</li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i><span> Blog </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="blogs-dashboard.html">Dashboard</a></li>
                        <li><a href="blogs-blog-list.html">Blog List</a></li>
                        <li><a href="blogs-blog-column.html">Blog Column</a></li>
                        <li><a href="blogs-blog-post.html">Blog Post</a></li>
                        <li><a href="blogs-blog-add.html">Add Blog</a></li>
                    </ul>
                </li> --}}

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

        <div class="help-box">
            <h5 class="text-muted m-t-0">For Help ?</h5>
            <p class=""><span class="text-custom">Email:</span> <br/> support@krdsport.com</p>
            <p class="m-b-0"><span class="text-custom">Call:</span> <br/> (+91) 9999999989</p>
        </div>

    </div>
    <!-- Sidebar -left -->

</div>