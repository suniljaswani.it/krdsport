@extends('admin.template.layout')

@section('title', 'Admin Managers')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-title-box">
			<h4 class="page-title">Admin Managers</h4>
			<ol class="breadcrumb p-0 m-0">
				<li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
				<li class="active"> Managers </li>
			</ol>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		@if (session('success'))
		<div class="alert alert-success"> {{ session('success') }}</div>
		@endif
		<div class="text-right m-b-10">
			<a href="{{ route('admin-manager-create') }}" class="btn btn-primary btn-sm">Create New</a>
		</div>
		<div class="card-box table-responsive">

			<table id="datatable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Status</th>
						<th>Created At</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@php $i = 1; @endphp
					@foreach ($admins as $admin)
					<tr>
						<td>{{ $i }}</td>
						<td>{{ $admin->name }}</td>
						<td>{{ $admin->email }}</td>
						<td>{{ $admin->mobile }}</td>
						<td>
							@if ($admin->status == 1)
								<span class="label label-purple"> Active</span>
							@else
								<span class="label label-danger"> In Active</span>
							@endif
						</td>
						<td>{{ \Carbon\Carbon::parse($admin->created_at)->format('d M Y') }}</td>
						<td>
							<a href="{{ route('admin-manager-update', ['id' => $admin->id ]) }}" class="btn btn-xs btn-purple"> Edit</a>
						</td>
					</tr>
					@php $i++; @endphp
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@stop