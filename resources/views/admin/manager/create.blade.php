@extends('admin.template.layout')

@section('title', 'Create Admin Manager')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-title-box">
			<h4 class="page-title">Create Admin Manager</h4>
			<ol class="breadcrumb p-0 m-0">
				<li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
				<li> <a href="{{ route('admin-manager-view') }}">Admin Managers</a> </li>
				<li class="active"> Create </li>
			</ol>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		@if (session('errors'))
		<div class="alert alert-danger">
			<ul>
				@foreach (session('errors')->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="card-box">
			<form action="" method="post" role="form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="form-group col-md-4">
						<label>Name</label>
						<input type="text" name="name" class="form-control" value="{{ old('name') }}">
					</div>
					<div class="form-group col-md-4">
						<label>Mobile</label>
						<input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}">
					</div>

					<div class="form-group col-md-12 text-center"> <hr> </div>
					<div class="form-group col-md-4">
						<label>Email</label>
						<input type="email" name="email" class="form-control" value="{{ old('email') }}" autocomplete="off">
					</div>
					<div class="form-group col-md-4">
						<label>Password</label>
						<input type="password" name="password" class="form-control" value="" autocomplete="off">
					</div>

					<div class="form-group col-md-12 text-center"> <hr> </div>
					<div class="form-group col-md-8">
						<label>Pages</label>
						<select name="routes[]" class="multi-select" multiple="" id="routeSelection" >
							@foreach ($routes as $route)
							<option value="{{ $route['key'] }}">{{ $route['value'] }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-md-12 text-center">
						<button class="btn btn-danger"> Create </button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@stop()

@section('import-css')
<link href="/admin-assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
@stop

@section('import-javascript')
<script type="text/javascript" src="/admin-assets/plugins/multiselect/js/jquery.multi-select.js"></script>
@stop

@section('page-javascript')
<script>
	jQuery(document).ready(function () {
		$('#routeSelection').multiSelect();
	})
</script>
@stop