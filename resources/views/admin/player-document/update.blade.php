@extends('admin.template.layout')

@section('title', 'Update Player Document')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">
                    Update Player Document For <span class="text-danger" >{{ $document->player->firstname.' '.$document->player->lastname }}</span>
                </h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
                    <li> <a href="{{ route('admin-player-document-list') }}">Player Document</a> </li>
                    <li class="active"> Update </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('errors'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach (session('errors')->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-box">
                <form action="" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Type</label>
                            <input type="text" name="pin" class="form-control" value="{{ $document->documentType->name_english }}" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Image</label><br/>
                            <a href="{{ env('PLAYER_DOCUMENT_URL').$document->player->id.'/'.$document->image }}" class="btn btn-icon waves-effect waves-light btn-primary btn-sm m-b-5" target="_blank">Click here To view Image</a>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Status</label>
                            <select name="status" class="form-control" required>
                                <option value="1" {{ $document->status == 1 ? 'selected' : '' }}>Pending</option>
                                <option value="2"  {{ $document->status == 2 ? 'selected' : '' }}>Approved</option>
                                <option value="3"  {{ $document->status == 3 ? 'selected' : '' }}>Rejected</option>
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <label>Remarks</label>
                            <textarea name="remarks" class="form-control" cols="3" rows="5">{{ $document->remarks }}</textarea>
                        </div>
                        <div class="form-group col-md-12 text-center m-t-20">
                            <button class="btn btn-danger"> Update </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop
