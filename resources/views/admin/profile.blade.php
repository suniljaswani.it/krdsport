@extends('admin.template.layout')

@section('title', 'Admin Profile')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-title-box">
			<h4 class="page-title">Admin Profile</h4>
			<ol class="breadcrumb p-0 m-0">
				<li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
				<li class="active"> Profile </li>
			</ol>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		@if (session('errors'))
		<div class="alert alert-danger">
			<ul>
				@foreach (session('errors')->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="card-box">
			<form action="" method="post" role="form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="form-group col-md-6">
						<label>Name</label>
						<input type="text" name="name" class="form-control" value="{{ Session::get('admin')->name }}">
					</div>
					<div class="form-group col-md-6">
						<label>Mobile</label>
						<input type="text" name="mobile" class="form-control" value="{{ Session::get('admin')->mobile }}">
					</div>

					<div class="form-group col-md-12 text-center"> <hr> </div>
					<div class="form-group col-md-6">
						<label>Email</label>
						<input type="email" name="email" class="form-control" value="{{ Session::get('admin')->email }}" autocomplete="off">
					</div>
					<div class="form-group col-md-6">
						<label>Password</label>
						<input type="password" name="password" class="form-control" value="" autocomplete="off">
					</div>
					<div class="form-group col-md-12 text-center">
						<button class="btn btn-danger"> Update </button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@stop()