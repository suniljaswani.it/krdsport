@extends('admin.template.layout')

@section('title', 'Participated Players')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Participated Players List</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
                    <li> <a href="javascript:void(0)">Participated Players</a> </li>
                    <li class="active"> List</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>

    <div class="row m-b-10">
        <div class="col-md-6 text-left">
            &nbsp;
        </div>
        <div class="col-md-6 text-right">
            <a href="{{ route('admin-apply-list') }}" class="btn btn-purple btn-sm m-r-10" title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="{{ route('admin-apply-list', ['download' => 'yes', 'status' => Request::get('status'), 'gender' => Request::get('gender'), 'final_eligibility_status' => Request::get('final_eligibility_status'), 'game_id' => Request::get('game_id'), 'search' => Request::get('search'), 'daterange' => Request::get('daterange')]) }}" class="btn btn-success btn-sm" title="Download">
                <i class="fa fa-download"></i>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="card-box">
            <div class="row m-b-20">
                <form action="" method="get">
                    <div class="col-md-3 m-t-10">
                        <div class="input-group" style="width: 100%">
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                            <input type="text" name="search" class="form-control" placeholder="Search Keyword" autocomplete="off" value="{{ Request::get('search') }}">
                        </div>
                    </div>
                    <div class="col-md-3 m-t-10">
                        <select name="game_id" class="form-control">
                            <option value="">Select Game</option>
                                @foreach ($games as $game)
                                    <option value="{{ $game->id }}" {{ Request::get('game_id') == $game->id ? 'selected' : '' }}> {{ $game->name }} </option>
                                @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 m-t-10">
                        <select name="status" class="form-control">
                            <option value="">Select Status</option>
                                <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}> Pending </option>
                                <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}> Approved </option>
                                <option value="3" {{ Request::get('status') == 3 ? 'selected' : '' }}> Rejected </option>
                        </select>
                    </div>
                    <div class="col-md-3 m-t-10">
                        <select name="gender" class="form-control">
                            <option value="">Select Gender</option>
                                <option value="1" {{ Request::get('gender') == 1 ? 'selected' : '' }}> Male </option>
                                <option value="2" {{ Request::get('gender') == 2 ? 'selected' : '' }}> Female </option>
                        </select>
                    </div>
                    <div class="col-md-3 m-t-10">
                        <select name="final_eligibility_status" class="form-control">
                            <option value="">Select Eligibility</option>
                                <option value="1" {{ Request::get('final_eligibility_status') == 1 ? 'selected' : '' }}> Eligible </option>
                                <option value="2" {{ Request::get('final_eligibility_status') == 2 ? 'selected' : '' }}> Not Eligible </option>
                        </select>
                    </div>
                    <div class="col-md-4 m-t-10">
                        <div class="input-group">
                            <span class="input-group-btn">
						        <button type="button" class="btn waves-effect waves-light btn-brown"><i class="fa fa-calendar"></i></button>
					        </span>
                            <input class="form-control date-range" type="text" name="daterange" placeholder="Date Range for Joining Date" readonly>
                        </div>
                    </div>
                    <div class="col-md-1 m-t-10">
                        <button class="btn btn-danger"> Search </button>
                    </div>
                </form>
            </div>
            <div class="table-responsive" id="PlayerDetails">
                <table class="table table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th></th>
                        <th> Actions </th>
                        <th> Joining Date </th>
                        <th> Name </th>
                        <th> Gender </th>
                        <th> Mobile </th>
                        <th> Game </th>
                        <th> Status </th>
                        <th>Final Eligibility</th>
                        <th> Remarks </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($participated_players as $key => $participated_player)
                        <tr id="player{{ $key+1 }}" class="accordion-toggle" data-toggle="collapse" data-parent="#PlayerDetails" data-target=".playerDetails{{ $key+1 }}">
                            <td>
                                <i class="indicator glyphicon glyphicon-chevron-down pull-left">
                            </td>
                            <td>
                                <a href="{{ route('admin-apply-edit', ['id' => $participated_player->id]) }}" target="_blank"><button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-info"><i class="mdi mdi-pencil"></i></button></a>
                            </td>
                            <td>{{ \Carbon\Carbon::parse($participated_player->created_at)->format('d M Y') }} </td>
                            <td>{{ $participated_player->player->firstname.' '.$participated_player->player->father_name.' '.$participated_player->player->lastname }}</td>
                            <td>{{ $participated_player->gender == 1 ? 'Male' : 'Female' }}</td>
                            <td>{{ $participated_player->player->mobile }} </td>
                            <td>{{ $participated_player->game->name }}</td>
                            <td>
                                @if ($participated_player->status == 1 )
                                    <button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-warning"><i class="mdi mdi-clock"></i></button>
                                @elseif ($participated_player->status == 2 )
                                    <button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-success"><i class="mdi mdi-check"></i></button>
                                @else
                                    <button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-danger"><i class="mdi mdi-close"></i></button>
                                @endif
                            </td>
                            <td>
                                @if ($participated_player->final_eligibility_status == 1 )
                                    <span class="badge badge-success">Eligible</span>
                                @else
                                    <span class="badge badge-danger">Not Eligible</span>
                                @endif
                            </td>
                            <td>
                                @if ($participated_player->remarks == NULL)
                                    -
                                @else
                                    <textarea rows="2" readonly>{{ $participated_player->remarks }}</textarea>
                                @endif
                            </td>
                        </tr>

                        <tr id="playerDetails{{ $key+1 }}" class="hidden-row">
                            <td colspan="10" class="hiddenRow">
                                <div class="accordion-body collapse playerDetails{{ $key+1 }}" id="accordion{{ $key+1 }}">
                                    <div class="table-responsive">
                                        <table class="table table-bordered ">
                                            <thead>
                                            <tr>
                                                <th> Current Roll Number </th>
                                                <th> College Stream </th>
                                                <th> HSC Pass Year </th>
                                                <th> SSC Pass Year </th>
                                                <th> College Start </th>
                                                <th> Current College Year </th>
                                                <th> Total College Year </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{ $participated_player->player->player_education->current_roll_no }}</td>
                                                <td>{{ $participated_player->player->player_education->college_stream }}</td>
                                                <td>{{ $participated_player->player->player_education->hsc_pass_year }}</td>
                                                <td>{{ $participated_player->player->player_education->ssc_pass_year }}</td>
                                                <td>{{ $participated_player->player->player_education->college_started_year }}</td>
                                                <td>{{ $participated_player->player->player_education->current_college_year }}</td>
                                                <td>{{ $participated_player->player->player_education->total_college_year }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $participated_players->appends(['search' => Request::get('search'), 'status' => Request::get('status'), 'gender' => Request::get('gender'), 'final_eligibility_status' => Request::get('final_eligibility_status'), 'game_id' => Request::get('game_id'), 'daterange' => Request::get('daterange') ])->links() }}
        </div>
    </div>
@stop

@section('page-javascript')
    <script type="text/javascript">
        @foreach ($participated_players as $key => $participated_player)
        $('#accordion{{ $key+1 }}').on('shown.bs.collapse', function () {
            $("#player{{ $key+1 }} i.indicator").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            $("#playerDetails{{ $key+1 }}").removeClass("hidden-row").addClass("show-row");
        });
        $('#accordion{{ $key+1 }}').on('hidden.bs.collapse', function () {
            $("#player{{ $key+1 }} i.indicator").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
            $("#playerDetails{{ $key+1 }}").removeClass("show-row").addClass("hidden-row");
        });
        @endforeach
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.date-range').daterangepicker({
                locale: {
                    format: 'DD MMM YYYY'
                },
                autoUpdateInput: false,
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-danger',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 15 Days': [moment().subtract(14, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@stop


@section('import-javascript')
    <script src="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/admin-assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="/admin-assets/plugins/custombox/js/legacy.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
    <link href="/admin-assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
@stop

@section('page-css')
    <style type="text/css">
        .custom-table{
            font-size: 12px;
        }
        .custom-table > tbody > tr > td, .custom-table > thead > tr > th {
            padding: 5px 10px;
        }
        .sweet-alert{
            z-index: 10009;
        }
        .sweet-overlay {
            z-index: 10008;
        }
    </style>
@endsection
