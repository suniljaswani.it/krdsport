@extends('admin.template.layout')

@section('title', 'Update Participated Player Status')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="page-title-box">
			<h4 class="page-title">Update Participated Player Status</h4>
			<ol class="breadcrumb p-0 m-0">
				<li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
				<li> <a href="{{ route('admin-player-detail') }}">Participants</a> </li>
				<li class="active"> Update Participated Player Status </li>
			</ol>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="row text-center">
    <div class="col-md-6 col-md-offset-3">
    @if (session('errors'))
        <div class="alert alert-danger">
            <ul>
                @foreach (session('errors')->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
    @endif
    </div>
</div>
<div class="row">
    <form action="" method="post" role="form" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="panel panel-border panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Update Game Status</h3>
            </div>
            <div class="panel-body">
				{{ csrf_field() }}
                <div class="col-md-3 form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>
                        <option value="1" {{ $participated_player->status == '1' ? 'selected' : '' }}>Pending</option>
                        <option value="2" {{ $participated_player->status == '2' ? 'selected' : '' }}>Approved</option>
                        <option value="3" {{ $participated_player->status == '3' ? 'selected' : '' }}>Rejected</option>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label>Final Eligibility Status</label>
                    <select name="final_eligibility_status" class="form-control" required>
                        <option value="1" {{ $participated_player->final_eligibility_status == '1' ? 'selected' : '' }}>Eligible</option>
                        <option value="2" {{ $participated_player->final_eligibility_status == '2' ? 'selected' : '' }}>Not Eligible</option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <label>Gender</label>
                    <select name="gender" class="form-control" required>
                        <option value="1" {{ $participated_player->gender == '1' ? 'selected' : '' }}>Male</option>
                        <option value="2" {{ $participated_player->gender == '2' ? 'selected' : '' }}>Female</option>
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <label>Remarks</label>
                    <textarea name="remarks" class="form-control" rows="2">{{ $participated_player->remarks }}</textarea>
                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-danger waves-effect w-md waves-light m-b-5">Update</button>
                </div>
            
            </div>
        </div>
    </div>
    </form>
	<div class="col-md-6">
		<div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Information</h3>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <label> First Name</label>
                    <input type="text" name="firstname" class="form-control" value="{{ $participated_player->player->firstname }}" readonly>
                </div>
                <div class="row form-group">
                    <label> Father Name</label>
                    <input type="text" name="father_name" class="form-control" value="{{ $participated_player->player->father_name }}" readonly>
                </div>
                <div class="row form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastname" class="form-control" value="{{ $participated_player->player->lastname }}" readonly>
                </div>
                <div class="row form-group">
                    <label>Gender</label>
                    <div class="radio">
                        <input type="radio" name="gender" value="{{ $participated_player->player->gender }}" checked>
                        <label>{{ $participated_player->player->gender == 1 ? 'Male' : 'Female' }}</label>
                    </div>
                </div>
                <div class="row form-group">
                    <label>Date of Birth</label>
                    <div class="input-group">
                        <input type="text" name="dob" value="{{ Carbon\Carbon::parse($participated_player->player->dob)->format('d-m-Y') }}" class="form-control" placeholder="dd-mm-yyyy" id="datepicker-autoclose1" readonly>
                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label>Aadhar Number</label>
                    <input type="text" name="aadhar_number" class="form-control" value="{{ $participated_player->player->aadhar_number }}" placeholder="Enter Your Aadhar Number" readonly>
                </div>
                <div class="row form-group">
                    <label>Mobile Number</label>
                    <input type="number" name="mobile" min="1" class="form-control" value="{{ $participated_player->player->mobile }}" placeholder="Enter Your Mobile Number" readonly>
                </div>
                <div class="row form-group">
                    <label>Email ID</label>
                    <input type="email" name="email" class="form-control" value="{{ $participated_player->player->email }}" readonly>
                </div>
                <div class="row form-group">
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" value="{{ $participated_player->player->password }}" readonly>
                </div>
                <div class="row form-group">
                    <label>Eligibility Status</label>
                    <select name="eligibility_status" class="form-control" readonly>
                        <option value="1" {{ $participated_player->player->eligibility_status == '1' ? 'selected' : 'Eligible' }}>Eligible</option>
                        <option value="2" {{ $participated_player->player->eligibility_status == '2' ? 'selected' : 'Not Eligible' }}>Not Eligible</option>
                    </select>
                </div>
            </div>
        </div>
	</div>
    <div class="col-md-6">
        <div class="panel panel-border panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Education Information</h3>
            </div>
            <div class="panel-body">
                <div class="row form-group">
                    <label>HSC Pass Year</label>
                    <input type="text" name="hsc_pass_year" value="{{ $participated_player->player->player_education->hsc_pass_year }}" class="form-control" readonly>
                </div>
                <div class="row form-group">
                    <label>SSC Pass Year</label>
                    <input type="text" name="ssc_pass_year" value="{{ $participated_player->player->player_education->ssc_pass_year }}" class="form-control" readonly>
                </div>
                <div class="row form-group">
                    <label>College Started Year</label>
                    <input type="text" name="college_started_year" value="{{ $participated_player->player->player_education->college_started_year }}" class="form-control" readonly>
                </div>
                <div class="row form-group">
                    <label>Current College Year</label>
                    <select name="current_college_year" class="form-control" readonly>
                        <option value="{{ $participated_player->player->player_education->current_college_year }}" >{{ $participated_player->player->player_education->current_college_year }}</option>
                    </select>
                </div>
                <div class="row form-group">
                    <label>Total College Year</label>
                    <select name="total_college_year" class="form-control" readonly>
                        <option value="{{ $participated_player->player->player_education->total_college_year }}"> {{ $participated_player->player->player_education->total_college_year }}</option>
                    </select>
                </div>
                <div class="row form-group">
                    <label>Current Roll Number</label>
                    <input type="text" name="current_roll_no" class="form-control" value="{{ $participated_player->player->player_education->current_roll_no }}" readonly>
                </div>
                <div class="row form-group">
                    <label>College Stream</label>
                    <input type="text" name="college_stream" class="form-control" value="{{ $participated_player->player->player_education->college_stream }}" readonly>
                </div>
            </div>
        </div>
    </div>
    
</div>

@stop

@section('import-javascript')
	<script src="/admin-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/admin-assets/plugins/switchery/switchery.min.js"></script>
@endsection

@section('page-javascript')
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$('#datepicker-autoclose1').datepicker({
				autoclose: true,
	            format: "dd-mm-yyyy",
	            startDate: "-99y",
	            endDate: "-18y"
		    });
            $('#datepicker-autoclose2, #datepicker-autoclose3').datepicker({
                autoclose: true,
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years"
            });
        });
	</script>
@endsection

@section('import-css')
    <link href="/admin-assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/admin-assets/plugins/switchery/switchery.min.css">
@endsection
