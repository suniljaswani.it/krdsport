@extends('admin.template.layout')

@section('title', 'Add Game')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">
                    Add Game <span class="text-danger" ></span>
                </h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
                    <li> <a href="{{ route('admin-game-list') }}">Game</a> </li>
                    <li class="active"> Add </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('errors'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach (session('errors')->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-box">
                <form action="" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Game Type</label>
                            <select name="type" class="form-control" required>
                                <option value="">Select Game Type</option>
                                @foreach ($gameTypes as $gameType)
                                    <option value="{{ $gameType->id }}">{{ $gameType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Name</label>
                            <input type="text" name="name" placeholder="Enter Game Name" class="form-control" value="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Start Date</label>
                            <div class="input-group">
                                <input type="text" name="start_date" value="" class="form-control" placeholder="dd-mm-yyyy" id="datepicker-autoclose1" required>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label>End Date</label>
                            <div class="input-group">
                                <input type="text" name="end_date" value="" class="form-control" placeholder="dd-mm-yyyy" id="datepicker-autoclose2" required>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Status</label>
                            <select name="status" class="form-control" required>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12 text-center m-t-20">
                            <button class="btn btn-danger"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop


@section('import-javascript')
	<script src="/admin-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection

@section('page-javascript')
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$('#datepicker-autoclose1').datepicker({
				autoclose: true,
	            format: "dd-mm-yyyy",
		    });
            $('#datepicker-autoclose2').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
            });
        });
	</script>
@endsection

@section('import-css')
    <link href="/admin-assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection