@extends('admin.template.layout')

@section('title', 'Game List')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Game List</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('admin-dashboard') }}">Dashboard</a> </li>
                    <li> <a href="javascript:void(0)">Game</a> </li>
                    <li class="active"> List </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>

    <div class="row m-b-10">
        <div class="col-md-6 text-left">
            &nbsp;
        </div>
        <div class="col-md-6 text-right">
            <a href="{{ route('admin-game-list') }}" class="btn btn-purple btn-sm m-r-10" title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="{{ route('admin-game-create')}}" class="btn btn-primary btn-sm" title="Download">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="card-box">
            <div class="row m-b-20">
                <form action="" method="get">
                    <div class="col-md-3">
                        <div class="input-group" style="width: 100%">
					        <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-search"></i></button>
					        </span>
                            <input type="text" name="search" class="form-control" placeholder="Search Keyword" autocomplete="off" value="{{ Request::get('search') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <select name="type" class="form-control">
                            <option value="">Select Game Type</option>
                            @foreach ($gameTypes as $gameType)
                                <option value="{{ $gameType->id }}">{{ $gameType->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 form-group">
                        <select name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                        <span class="input-group-btn">
						<button type="button" class="btn waves-effect waves-light btn-brown"><i class="fa fa-calendar"></i></button>
					</span>
                            <input class="form-control date-range" type="text" name="daterange" placeholder="Date Range for Start Date" readonly>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-danger"> Search </button>
                    </div>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th> Created Date </th>
                        <th> Name</th>
                        <th> Type </th>
                        <th> Start Date </th>
                        <th> End Date </th>
                        <th> Status </th>
                        <th> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($games as $game)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($game->created_at)->format('d M Y') }} </td>
                            <td>{{ $game->name }}</td>
                            <td>{{ $game->gameType->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($game->start_date)->format('d M Y') }} </td>
                            <td>{{ \Carbon\Carbon::parse($game->end_date)->format('d M Y') }} </td>
                            <td>
                                @if ($game->status == 1 )
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-game-update', ['id' => $game->id]) }}" class="btn btn-icon waves-effect waves-light btn-purple btn-sm m-b-5"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $games->appends(['search' => Request::get('search'), 'type' => Request::get('type'), 'status' => Request::get('status'), 'daterange' => Request::get('daterange') ])->links() }}
        </div>
    </div>


@stop

@section('page-javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.date-range').daterangepicker({
                locale: {
                    format: 'DD MMM YYYY'
                },
                autoUpdateInput: false,
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-danger',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 15 Days': [moment().subtract(14, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@stop


@section('import-javascript')
    <script src="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
@stop

