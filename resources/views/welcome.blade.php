<!DOCTYPE html>
<html>
    <head>
        <title>KRD Sport</title>
        <link href="/player-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Welcome to KRD Sport</div>
            </div>
            <div class="row">
                <center>
                    <a href="http://uat.krdsport.com/player" class="btn btn-success btn-lg"><b>Student Login</b></a>
                    &nbsp;
                    <a href="http://uat.krdsport.com/admin" class="btn btn-primary btn-lg"><b>Admin Login</b></a>
                </center>
            </div>
        </div>
        <!-- jQuery  -->
        <script src="/player-assets/js/jquery.min.js"></script>
        <script src="/player-assets/js/bootstrap.min.js"></script>
    </body>
</html>
