@extends('player.template.layout')

@section('title', 'Paticipated List')

@section('content')
    <div class="row">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Paticipated List
                    <a href="{{ route('player-apply-create') }}"><span class="pull-right text-inverse">Apply For a New Game</span></a>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        @if (session('success'))
                        <div class="alert alert-success"> {{ session('success') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Created On</th>
                                <th>Updated On</th>
                                <th>Type</th>
                                <th>Game Name</th>
                                <th>Status</th>
                                <th>Final Eligibility</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($participants as $participate)
                                <tr>
                                    <td>{{ $participate->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $participate->updated_at != NULL ? $participate->updated_at->format('d-m-Y') : '-' }}</td>
                                    <td>
                                        {{ $participate->game->gameType->name }}
                                    </td>
                                    <td>
                                        {{ $participate->game->name }}
                                    </td>
                                    <td>
                                        @if ($participate->status == 1)
                                            <span class="label label-warning">Pending</span>
                                        @elseif($participate->status == 2) 
                                            <span class="label label-success">Approved</span>
                                        @else                                           
                                            <span class="label label-danger">Rejected</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($participate->final_eligibility_status == 1)
                                            <span class="label label-success">Eligible</span>
                                        @else                                           
                                            <span class="label label-danger">Not Eligible</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($participate->remarks == NULL)
                                        -
                                        @else
                                            <textarea rows="2"  readonly>{{ $participate->remarks }}</textarea>
                                        @endif
                                    </td>
                                    <td>
                                         <a href="{{ route('player-apply-edit', ['id' => $participate->id]) }}" target="_blank"><button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-info"><i class="fa fa-pencil"></i></button></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $participants->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-css')
    <style>
        table > thead > tr > th {
            text-align: center;
        }
        table > tbody > tr > td {
            text-align: center;
        }

    </style>
@endsection