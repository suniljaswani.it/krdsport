@extends('player.template.layout')

@section('title', 'Apply Game | Player Panel')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-pink panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Apply Game
                        <a href="{{ route('player-apply-list') }}"> <button type="button" class="btn btn-inverse waves-effect w-xs waves-light pull-right">List of Applied Games</button></a>
                    </h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>Type of the Game</label>
                                <select class="form-control" name="type" id="typeselector" required>
                                    <option value="">Select Type of the Game</option>
                                    @foreach ($gameTypes as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            {{-- One Game For Id = 1 --}}
                            <div class="col-md-12 form-group spacetb-10" id="one" style="display:none">
                                <label> {{ $oneTypeName->name }} Game</label>
                                <select class="form-control" name="game_for_type_1" id="onefield" required>
                                    <option value="">Select Game</option>
                                    @foreach ($gameOnes as $one)
                                    <option value="{{ $one->id }}">{{ $one->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            {{-- Two Game For Id = 2 --}}
                            <div class="col-md-12 form-group spacetb-10" id="two" style="display:none">
                                <label> {{ $twoTypeName->name }} Game</label>
                                <select class="form-control" name="game_for_type_2" id="twofield" required>
                                    <option value="">Select Game</option>
                                    @foreach ($gameTwos as $two)
                                        <option value="{{ $two->id }}">{{ $two->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row text-center">
                            <button type="submit" class="btn btn-danger m-t-10">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('page-javascript')
    <script type="text/javascript">
        // Selection of game type based game
        $(function () {
            $("#typeselector").change(function () {
                if ($(this).val() == "1") {
                    $("#one").show();
                    $("#two").hide();
                    $("#twofield").removeAttr('required');
                } else {
                    $("#two").show();
                    $("#one").hide();
                    $("#onefield").removeAttr('required');
                }
            });
        });
    </script>
@endsection
