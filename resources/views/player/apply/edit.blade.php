@extends('player.template.layout')

@section('title', 'Update Applied Game | Player Panel')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-pink panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Update Applied Game
                        <a href="{{ route('player-apply-list') }}"> <button type="button" class="btn btn-inverse waves-effect w-xs waves-light pull-right">List of Applied Games</button></a>
                    </h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>Type of the Game</label>
                                <input type="text" name="type" class="form-control" value="{{ $participate_info->game->gameType->name }}" readonly>
                            </div>
                            <div class="col-md-12 form-group">
                                <label>Game</label>
                                <select class="form-control" name="game_id" id="typeselector" required>
                                    <option value="">Select Type of the Game</option>
                                    @foreach ($games as $game)
                                        <option value="{{ $game->id }}" {{ $participate_info->game_id == $game->id ? 'selected' : '' }}>{{ $game->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row text-center">
                            <button type="submit" class="btn btn-danger m-t-10">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

