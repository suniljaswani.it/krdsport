<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Associate Login For Seyago India">
    <meta name="author" content="Seyago India">
    <link rel="shortcut icon" href="/website-assets/images/favicon.png">

    <title>Register Step 3 - Seyago India</title>

    <link href="/associate-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <style type="text/css" media="all">
        .account-pages {
            background: url(/associate-assets/images/register-bg.png)  repeat center;
            position: absolute;
            height: 100%;
            width: 100%;
        }
        .wrapper-page {
            margin: 2% auto;
            position: relative;
            width: 50%;
        }
        .spacetb-10{
            margin: 10px 0;
        }
        @media only screen and (max-width: 767px){
            .wrapper-page {
                width: 90%;
            }
        }
    </style>
    <script src="/associate-assets/js/modernizr.min.js"></script>

</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
    	<div class="m-t-40 card-box">
            <div class="text-center spacetb-10">
                @if (session('errors'))
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="text-center m-b-20">
                <a href="{{ route('associate-login') }}" class="logo">
                    <img src="/website-assets/images/seyago-logo.png" alt="">
                </a>
            </div>
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Register Step 3</h4>
                {{-- <span>Please let us know who introduced you to Seyago.</span> --}}
            </div>
            <div class="panel-body">
                <form class="form-group m-t-20" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Sponsor: </label> {{ $sponsor->user_detail->full_name }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Upline: </label>{{ $upline->user_detail->full_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Leg: </label> {{ $user_detail->leg }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Name: </label> {{ $user_detail->title.'. '.$user_detail->first_name.' '.$user_detail->last_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>D.O.B.: </label> {{ $user_detail->dob }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Email: </label> {{ $user_detail->email }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Mobile: </label> {{ $user_detail->mobile }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Address: </label> {{ $user_detail->address }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Landmark: </label> {{ $user_detail->landmark }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>City: </label>  {{ $user_detail->city }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>State: </label>  {{ App\Models\State::where('id',$user_detail->state)->first()->name }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Pincode: </label> {{ $user_detail->pincode }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Pan No.: </label> {{ $user_detail->pan_no }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Nominee Name: </label> {{ $user_detail->nominee_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Nominee Relation: </label> {{ $user_detail->nominee_relation }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Nominee D.O.B.: </label> {{ $user_detail->nominee_birth_date }}
                        </div>
                    </div>
                    <div class="row m-b-30 m-t-20">
                        <div class="text-center">
                            <h4 class="text-uppercase font-bold m-b-0">Seyago Account Details</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Username: </label> {{ $user_detail->username }}
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Password: </label> {{ $user_detail->password }}
                        </div>
                    </div>
                    <div class="row m-t-30">
                        <div class="form-group text-center col-md-3 col-md-offset-3">
                            <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" onclick="history.go(-2);">
                                Back <i class="fa fa-hand-o-left"></i>
                            </button>
                        </div>
                        <div class="form-group text-center col-md-3">
                            <button class="btn btn-danger btn-bordred btn-block waves-effect waves-light" type="submit">
                                Submit <i class="fa fa-hand-o-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Already have an account? <a href="{{ route('associate-login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card-box-->
    </div>
    <!-- end wrapper page -->

	<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/associate-assets/js/jquery.min.js"></script>
    <script src="/associate-assets/js/bootstrap.min.js"></script>
    <script src="/associate-assets/js/detect.js"></script>
    <script src="/associate-assets/js/fastclick.js"></script>
    <script src="/associate-assets/js/jquery.slimscroll.js"></script>
    <script src="/associate-assets/js/jquery.blockUI.js"></script>
    <script src="/associate-assets/js/waves.js"></script>
    <script src="/associate-assets/js/wow.min.js"></script>
    <script src="/associate-assets/js/jquery.nicescroll.js"></script>
    <script src="/associate-assets/js/jquery.scrollTo.min.js"></script>

    <script src="/associate-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- App js -->
    <script src="/associate-assets/js/jquery.core.js"></script>
    <script src="/associate-assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        jQuery('#datepicker-autoclose1,#datepicker-autoclose2').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd/mm/yyyy"
        });
    </script>

</body>
</html>