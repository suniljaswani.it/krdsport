<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!-- User -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ App\Models\Player::whereId(Session::get('player')->id)->first()->image ? env('PROFILE_IMAGE_URL').App\Models\Player::whereId(Session::get('player')->id)->first()->image : '/player-assets/images/users/user.svg' }}" alt="user-img" title="{{ Session::get('player')->firstname.' '.Session::get('player')->lastname }}" class="img-circle img-thumbnail img-responsive" style="height: 88px;width: 88px;">
                {{-- <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div> --}}
            </div>
            <h5><a href="javascript:void(0)">{{ Session::get('player')->firstname.' '.Session::get('player')->lastname }}</a> </h5>
            {{-- <h6>({{ Session::get('player')->tracking_id }}) </h6> --}}
            <ul class="list-inline">
                {{-- <li>
                    <a href="#" >
                        <i class="zmdi zmdi-settings"></i>
                    </a>
                </li> --}}
                <li>
                    <a href="{{ route('player-logout') }}" title="logout" class="text-custom">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                </li>

            </ul>
        </div>
        <!-- End User -->

        <!--- Sidebar -->
        <div id="sidebar-menu">
            <ul>
            	<li class="text-muted menu-title">Navigation</li>
                <li>
                    <a href="{{ route('player-dashboard') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i> <span> Home </span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-gamepad"></i> <span> Game Apply </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" style="">
                        <li><a href="{{ route('player-apply-create') }}"> Create </a></li>
                        <li><a href="{{ route('player-apply-list') }}"> List </a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-pdf-o"></i> <span> Player Documents </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" style="">
                        <li><a href="{{ route('player-document-upload') }}"> Upload </a></li>
                        <li><a href="{{ route('player-document-list') }}"> List </a></li>
                    </ul>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-account"></i> <span> My Account Settings </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" style="">
                        <li><a href="{{ route('player-profile') }}"> My Profile </a></li>
                        <li><a href="{{ route('player-profile-password') }}"> Password </a></li>
                    </ul>
                </li>

                {{-- <li>
                    <a href="{{ route('player-quick-support') }}" class="waves-effect"><i class="fa fa-ticket"></i> <span> Quick Support </span> </a>
                </li>

                <li>
                    <a href="{{ route('player-faqs') }}" class="waves-effect"><i class="fa fa-question"></i> <span> FAQs </span> </a>
                </li>

                <li>
                    <a href="{{ route('player-terms', ['id' => 1]) }}" class="waves-effect"><i class="fa fa-file-o"></i> <span> T&C </span> </a>
                </li> --}}

            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
</div>