<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Player Panel For KRD Sport">
    <meta name="author" content="Axoip Inc">
    <link rel="shortcut icon" href="/player-assets/images/favicon.ico">
    <title>@yield('title') - KRD Sport</title>

    <!-- App css -->
    @yield('import-css')
    <link href="/player-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/mystyle.css" rel="stylesheet" type="text/css" />
    <script src="/player-assets/js/modernizr.min.js"></script>
    @yield('page-css')

</head>

<body class="fixed-left">
    <div id="wrapper">
        @include('player.template.header')
        @include('player.template.sidemenu')
        <div class="content-page">
            <div class="content">
                <div class="container">
                    @yield('content')
                </div>
            </div>
            <footer class="footer">
                {{ date('Y') }} © KRD Sport.
            </footer>
        </div>
        @include('player.template.right-sidemenu')
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/player-assets/js/jquery.min.js"></script>
    <script src="/player-assets/js/bootstrap.min.js"></script>
    <script src="/player-assets/js/detect.js"></script>
    <script src="/player-assets/js/fastclick.js"></script>
    <script src="/player-assets/js/jquery.slimscroll.js"></script>
    <script src="/player-assets/js/jquery.blockUI.js"></script>
    <script src="/player-assets/js/waves.js"></script>
    <script src="/player-assets/js/jquery.nicescroll.js"></script>
    <script src="/player-assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="/player-assets/js/jquery.core.js"></script>
    <script src="/player-assets/js/jquery.app.js"></script>

    @yield('import-javascript')

    @yield('page-javascript')
</body>
</html>