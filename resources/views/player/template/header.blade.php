<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{ route('player-dashboard') }}" class="logo">
            {{-- <img src="/work/other_projects/mlm_seyagoindia/seyagoindia/public/website-assets/images/seyago-logo.png" style="height:65px;padding:5px;" alt=""> --}}
            <span>KRD<span> Sport</span></span>
            <i class="zmdi zmdi-layers"></i>
        </a>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">

            <!-- Page title -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <button class="button-menu-mobile open-left">
                        <i class="zmdi zmdi-menu"></i>
                    </button>
                </li>
                <li>
                    {{-- <h4 class="page-title text-center">Welcome To Associate Panel</h4> --}}
                </li>
            </ul>

            <h4 class="page-title text-center hide-on-mobile">Welcome {{ Session::get('player')->firstname.' '.Session::get('player')->lastname }} on Your Home</h4>

        </div><!-- end container -->
    </div><!-- end navbar -->
</div>
