@extends('associate.template.layout')

@section('title', 'Wallet Password Update')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <ul>
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                </ul>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger"> {{ session('error') }}</div>
            @endif
        </div>
    </div>
    <form role="form" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-danger panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Wallet Password</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <label>Old Wallet Password</label>
                            <input type="password" name="old_wallet_password" class="form-control" value="" required>
                        </div>
                        <div class="row form-group">
                            <label>New Wallet Password</label>
                            <input type="password" name="new_wallet_password" class="form-control" value="" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row m-t-30 m-b-20 text-center">
            <button type="submit" class="btn btn-danger waves-effect w-md waves-light">Update Wallet Password</button>
        </div>
    </form>
@endsection

