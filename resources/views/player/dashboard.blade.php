@extends('player.template.layout')

@section('title', 'Player Home')

@section('content')
    {{-- <div class="row animated fadeInLeft">
        <img src="/player-assets/images/bg/player-bg-4.jpg" class="representative-bg" alt="">
    </div> --}}
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger"> {{ session('error') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 animated tada">
            <a href="{{ route('player-apply-list')}}">
                <div class="card-box widget-user">
                    <div>
                        <img src="/player-assets/images/flaticon/olympic-games.svg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5 font-600"> Apply</h4>
                            <p class="text-muted m-b-5 font-13">Participate In Games</p>
                            <small class="text-success"><b>View More</b></small>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6 animated jackInTheBox">
            <a href="{{ route('player-profile') }}">
                <div class="card-box widget-user">
                    <div>
                        <img src="/player-assets/images/users/user.svg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5 font-600"> Profile</h4>
                            <p class="text-muted m-b-5 font-13">Update Profile</p>
                            <small class="text-success"><b>View More</b></small>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6 animated jello">
            <a href="{{ route('player-document-list') }}">
                <div class="card-box widget-user">
                    <div>
                        <img src="/player-assets/images/flaticon/document.svg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5 font-600">Documents</h4>
                            <p class="text-muted m-b-5 font-13">Upload/List Doc.</p>
                            <small class="text-success"><b>View More</b></small>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    </div>
    
@endsection

@section('import-javascript')
    <script src="/player-assets/plugins/bootstrap-touch-slider/touch-slider.js"></script>
    <script src="/player-assets/plugins/tinymce/tinymce.min.js"></script>
    <script src="/player-assets/plugins/count-down/jquery.lwtCountdown-1.0.js"></script>
@endsection

@section('page-javascript')
    {{-- <script type="text/javascript">
        $(document).ready(function () {
            if($("#elm1").length > 0){
                tinymce.init({
                    selector: "textarea#elm1",
                    theme: "modern",
                    height:200,
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ],
                    setup: function (editor) {
                        editor.on('change', function () {
                            editor.save();
                        });
                    }
                });
            }
        });
    </script> --}}
    {{-- <script type="text/javascript">
        //        Slider
        //Initialise Bootstrap Carousel Touch Slider
        // Curently there are no option available.
        // $('#bootstrap-touch-slider').bsTouchSlider();
    </script> --}}
@endsection


@section('page-css')
    <style>
        /* .representative-bg
        {
            width: 100%;
            height:500px;
            margin-bottom: 20px;
        } */
        /* #count-down .clock-presenter .digit
        {
            font-size: 30px;
            height: 50px;
            color: red;
        }
        #count-down .clock-presenter .note
        {
            padding-top: 0;
        }
        #count-down {
            margin-top: 2px;
            border: 1px solid;
            background: #fff;
            margin-bottom: 5px;
        } */
        @media only screen and (max-width: 767px)
        {

            /* #count-down .clock-presenter {
                padding: 0;
                width: 25%;
                float: left;
            }
            #count-down
            {
                margin-bottom: 35px;
            } */
        }
        /* Slider */

        /* .bs-slider{
            overflow: hidden;
            max-height: 300px;
            position: relative;
            background: #000000;
        }
        .bs-slider:hover {
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }
        .bs-slider:active {
            cursor: -moz-grabbing;
            cursor: -webkit-grabbing;
        }
        .bs-slider .bs-slider-overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.40);
        }
        .bs-slider > .carousel-inner > .item > img,
        .bs-slider > .carousel-inner > .item > a > img {
            margin: auto;
            width: 100% !important;
        } */

        /********************
        *****Slide effect
        **********************/

        .fade {
            opacity: 1;
        }
        .fade .item {
            top: 0;
            z-index: 1;
            opacity: 0;
            width: 100%;
            position: absolute;
            left: 0 !important;
            display: block !important;
            -webkit-transition: opacity ease-in-out 1s;
            -moz-transition: opacity ease-in-out 1s;
            -ms-transition: opacity ease-in-out 1s;
            -o-transition: opacity ease-in-out 1s;
            transition: opacity ease-in-out 1s;
        }
        .fade .item:first-child {
            top: auto;
            position: relative;
        }
        .fade .item.active {
            opacity: 1;
            z-index: 2;
            -webkit-transition: opacity ease-in-out 1s;
            -moz-transition: opacity ease-in-out 1s;
            -ms-transition: opacity ease-in-out 1s;
            -o-transition: opacity ease-in-out 1s;
            transition: opacity ease-in-out 1s;
        }

        /*---------- LEFT/RIGHT ROUND CONTROL ----------*/
        .control-round .carousel-control {
            top: 47%;
            opacity: 0;
            width: 45px;
            height: 45px;
            z-index: 100;
            color: #ffffff;
            display: block;
            font-size: 24px;
            cursor: pointer;
            overflow: hidden;
            line-height: 43px;
            text-shadow: none;
            position: absolute;
            font-weight: normal;
            background: transparent;
            -webkit-border-radius: 100px;
            border-radius: 100px;
        }
        .control-round:hover .carousel-control{
            opacity: 1;
        }
        .control-round .carousel-control.left {
            left: 1%;
        }
        .control-round .carousel-control.right {
            right: 1%;
        }
        .control-round .carousel-control.left:hover,
        .control-round .carousel-control.right:hover{
            color: #fdfdfd;
            background: rgba(0, 0, 0, 0.5);
            border: 0px transparent;
        }
        .control-round .carousel-control.left>span:nth-child(1){
            left: 35%;
        }
        .control-round .carousel-control.right>span:nth-child(1){
            right: 35%;
        }
        .carousel-control span {
            top: 20%;
        }
    </style>
@endsection

@section('import-css')
    <link rel="stylesheet" href="/player-assets/plugins/animation/animate.css" />
@endsection