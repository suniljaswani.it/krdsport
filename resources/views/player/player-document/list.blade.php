@extends('player.template.layout')

@section('title', 'My Documents')

@section('content')
    <div class="row">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    My Documents
                    <a href="{{ route('player-document-upload') }}"><span class="pull-right text-inverse">Upload Document</span></a>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row m-b-20">
                    <form method="get">
                        <div class="col-md-3 form-group">
                            <select class="form-control" name="type" >
                                <option value="">Select Document Type</option>
                                @foreach ($documentTypes as $type)
                                    <option value="{{ $type->id }}">{{ $type->name_gujarati }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <select name="status" class="form-control">
                                <option value="">Search By Status</option>
                                <option value="1">Pending</option>
                                <option value="2">Approved</option>
                                <option value="3">Rejected</option>
                            </select>
                        </div>
                        <div class="col-md-2 form-group">
                            <button class="btn btn-danger waves-effect waves-light">Search</button>
                        </div>
                        <div class="col-md-1 pull-right">
                            <a href="{{ route('player-document-list')  }}" class="btn btn-icon waves-effect waves-light btn-primary m-b-5"><i class="fa fa-refresh"></i></a>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Created On</th>
                                <th>Type</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($documents as $document)
                                <tr>
                                    <td>{{ $document->created_at->format('d-m-Y') }}</td>
                                    <td>
                                        {{ $document->documentType->name_gujarati }}
                                    </td>
                                    <td>
                                        <a href="{{ env('PLAYER_DOCUMENT_URL').$document->player->id.'/'.$document->image }}" class="btn btn-icon waves-effect waves-light btn-primary btn-sm m-b-5" target="_blank"><i class="fa fa-image"></i></a>
                                    </td>
                                    <td>
                                        @if($document->status == 1)
                                            <span class="label label-warning">Pending</span>
                                        @elseif($document->status == 2)
                                            <span class="label label-success">Success</span>
                                        @else
                                            <span class="label label-danger">Rejected</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($document->remarks != null)
                                            <textarea class="form-control" cols="1" rows="1" readonly>{{ $document->remarks }}</textarea>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $documents->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-css')
    <style>
        table > thead > tr > th {
            text-align: center;
        }
        table > tbody > tr > td {
            text-align: center;
        }

    </style>
@endsection