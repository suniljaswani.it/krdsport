@extends('player.template.layout')

@section('title', 'Document Upload')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-pink panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Document Upload
                        <a href="{{ route('player-document-list') }}"> <button type="button" class="btn btn-inverse waves-effect w-xs waves-light pull-right">List of Document</button></a>
                    </h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>Type</label>
                                <select class="form-control" name="type" required>
                                    <option value="">Select Document Type</option>
                                    @foreach ($documentTypes as $type)
                                        <option value="{{ $type->id }}">{{ $type->name_gujarati }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label>Image</label>
                                <input type="file" name="image" accept="image/x-png,image/jpeg" required/>
                                <span class="help-block text-danger">
                                    (Upload JPG and PNG Files only and Maximum Size is 2 MB.)
                                </span>
                            </div>
                        </div>
                        <div class="row text-center">
                            <button type="submit" class="btn btn-danger m-t-10">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

