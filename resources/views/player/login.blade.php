<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Player Login For KRD Sport">
    <meta name="author" content="Axoip Inc">
    <link rel="shortcut icon" href="/player-assets/images/favicon.ico">

    <title>Player Login - KRD Sport</title>

    <link href="/player-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/player-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="all">
        .account-pages {
            background: url(/player-assets/images/stardust.png)  repeat center;
            position: absolute;
            height: 100%;
            width: 100%;
        }

    </style>
    <script src="/player-assets/js/modernizr.min.js"></script>
</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">

    	<div class="m-t-40 card-box">
            <div class="text-center m-b-20">
                 <h3><b>KRD Sport</b></h3>
                {{-- <a href="{{ route('player-login') }}" class="logo">
                    <img src="/website-assets/images/seyago-logo.png" alt="">
                </a> --}}
            </div>
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        @if (session('errors'))
                            <div class="alert alert-danger">
                                @foreach (session('errors')->all() as $error)
                                    <span class="text-center">{{ $error }}</span>
                                @endforeach
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success"> {{ session('success') }}</div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger"> {{ session('error') }}</div>
                        @endif
                    </div>
                </div>
                <form class="form-horizontal m-t-20" method="post">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input type="number" name="mobile" class="form-control" placeholder="Mobile Number" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>

                    {{-- <div class="form-group ">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-custom">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>

                        </div>
                    </div> --}}

                    <div class="form-group text-center m-t-30">
                        <div class="col-xs-12">
                            <button class="btn btn-danger waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>

                    {{-- <div class="form-group m-t-30 m-b-0 text-center">
                        <div class="col-sm-12">
                            <a href="{{ route('player-forgot-password') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot password?</a>
                        </div>
                    </div> --}}

                </form>
                {{-- <div class="row m-t-30">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Don't have an account? <a href="{{ route('player-register1') }}" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- end card-box-->
    </div>
    <!-- end wrapper page -->

	<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/player-assets/js/jquery.min.js"></script>
    <script src="/player-assets/js/bootstrap.min.js"></script>
    <script src="/player-assets/js/detect.js"></script>
    <script src="/player-assets/js/fastclick.js"></script>
    <script src="/player-assets/js/jquery.slimscroll.js"></script>
    <script src="/player-assets/js/jquery.blockUI.js"></script>
    <script src="/player-assets/js/waves.js"></script>
    <script src="/player-assets/js/wow.min.js"></script>
    <script src="/player-assets/js/jquery.nicescroll.js"></script>
    <script src="/player-assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="/player-assets/js/jquery.core.js"></script>
    <script src="/player-assets/js/jquery.app.js"></script>

</body>
</html>