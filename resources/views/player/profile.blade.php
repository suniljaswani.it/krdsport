@extends('player.template.layout')

@section('title', 'Profile Update')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
            <div class="alert alert-danger">
                @foreach (session('errors')->all() as $error)
                    <span class="text-center">{{ $error }}</span><br/>
                @endforeach
            </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>
    <form role="form" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-custom panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Personal Information</h3>
                </div>
                <div class="panel-body">
                    <div class="row form-group">
                        <label>First Name</label>
                        <input type="text" name="firstname" class="form-control" value="{{ $player->firstname }}" required>
                    </div>
                    <div class="row form-group">
                        <label>Father Name</label>
                        <input type="text" name="father_name" class="form-control" value="{{ $player->father_name }}" required>
                    </div>
                    <div class="row form-group">
                        <label>Last Name</label>
                        <input type="text" name="lastname" class="form-control" value="{{ $player->lastname }}" required>
                    </div>
                    <div class="row form-group">
                        <label>Gender</label>
                        <div class="radio">
                            <input type="radio" name="gender" value="1" {{ $player->gender == 1 ? 'checked' : '' }}>
                            <label>Male</label>
                        </div>
                        <div class="radio">
                            <input type="radio" name="gender" value="2" {{ $player->gender == 2 ? 'checked' : '' }}>
                            <label>Female</label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label>Date of Birth</label>
                        <div class="input-group">
                            <input type="text" name="dob" value="{{ Carbon\Carbon::parse($player->dob)->format('d-m-Y') }}" class="form-control" placeholder="Birth Date" id="datepicker-autoclose1" required>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label>Aadhar Number</label>
                        <input type="number" name="aadhar_number" class="form-control" value="{{ $player->aadhar_number }}" placeholder="Enter Your Aadhar Number">
                    </div>
                    <div class="row form-group">
                        <label>Mobile Number</label>
                        <p>{{ $player->mobile }}</p>
                        {{-- <input type="number" name="mobile" class="form-control" value="{{ $player->mobile }}" placeholder="Enter Your Mobile Number"> --}}
                    </div>
                    <div class="row form-group">
                        <label>Email ID</label>
                        <input type="email" name="email" class="form-control" value="{{ $player->email }}" >
                    </div>
                    {{-- <div class="row form-group">
                        <label>Profile Picture</label>
                        <input type="file" name="profile_image">
                        <div>
                            <img src="{{ $player->image ? env('PROFILE_IMAGE_URL').$player->image : '/player-assets/images/users/user.svg' }}" class="profile-image" >
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Education Information</h3>
                </div>
                <div class="panel-body">
                    <div class="row form-group">
                        <label>SSC Pass Year</label>
                        <div class="input-group">
                            <input type="text" name="ssc_pass_year" value="{{ $player->player_education ? Carbon\Carbon::parse($player->player_education->ssc_pass_year)->format('d-m-Y') : '' }}" class="form-control" placeholder="SSC Pass Year" id="datepicker-autoclose4" required>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label>HSC Pass Year</label>
                        <div class="input-group">
                            <input type="text" name="hsc_pass_year" value="{{ $player->player_education ? Carbon\Carbon::parse($player->player_education->hsc_pass_year)->format('d-m-Y') : '' }}" class="form-control" placeholder="HSC Pass Year" id="datepicker-autoclose2" required>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label>College Started Year</label>
                        <div class="input-group">
                            <input type="text" name="college_started_year" value="{{ $player->player_education ? Carbon\Carbon::parse($player->player_education->college_started_year)->format('d-m-Y') : '' }}" class="form-control" placeholder="College Started Year" id="datepicker-autoclose3" required>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label>Current College Year</label>
                        <select name="current_college_year" class="form-control" >
                            @for ($i = 1; $i <= 5; $i++)
                                <option value="{{ $i }}" 
                                    @if($player->player_education)
                                        {{ $player->player_education->current_college_year == $i ? 'selected' : '' }}> {{ $i }}
                                    @else
                                        >{{ $i }}
                                    @endif
                                </option>
                            @endfor
                        </select>
                    </div>
                    <div class="row form-group">
                        <label>Total College Year</label>
                        <select name="total_college_year" class="form-control" >
                            @if ($player->player_education)
                                <option value="2" {{ $player->player_education->total_college_year == 2 ? 'selected' : '' }} >2</option>
                                <option value="3" {{ $player->player_education->total_college_year == 3 ? 'selected' : '' }} >3</option>
                                <option value="5" {{ $player->player_education->total_college_year == 5 ? 'selected' : '' }} >5</option>
                            @else
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="5" >5</option>
                            @endif
                        </select>
                    </div>
                    <div class="row form-group">
                         <label>Current Roll Number</label>
                        <input type="text" name="current_roll_no" class="form-control" value="{{ $player->player_education ? $player->player_education->current_roll_no : '' }}" >
                    </div>
                    <div class="row form-group">
                        <label>College Stream</label>
                        <select name="college_stream" class="form-control" >
                        @if ($player->player_education)
                                <option value="arts" {{ $player->player_education->college_stream == 'arts' ? 'selected' : '' }} >arts</option>
                                <option value="commerce" {{ $player->player_education->college_stream == 'commerce' ? 'selected' : '' }} >commerce</option>
                                <option value="science" {{ $player->player_education->college_stream == 'science' ? 'selected' : '' }} >science</option>
                            @else
                                <option value="arts" >arts</option>
                                <option value="commerce" >commerce</option>
                                <option value="science" >science</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-30 m-b-20 text-center">
        <button type="submit" class="btn btn-danger waves-effect w-md waves-light">Update</button>
    </div>
    </form>
@endsection

@section('import-javascript')
    <script src="/player-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
@endsection

@section('page-javascript')
    <script type="text/javascript">
        jQuery('#datepicker-autoclose1').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd-mm-yyyy"
        });
        jQuery('#datepicker-autoclose2, #datepicker-autoclose3, #datepicker-autoclose4').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
        });
    </script>
@endsection

@section('import-css')
    <link href="/player-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('page-css')
    {{-- <style type="text/css">
        .profile-image{
            width: 100px;
            height:100px;
            float: right;
            border-radius: 50%;
            margin-top: -42px;
            border: 1px solid #eee;
            margin-right: 50px;
        }
        @media only screen and (max-width: 767px){
            .profile-image{
                width: 70px;
                height: 70px;
                float: right;
                border-radius: 50%;
                margin-top: -40px;
                border: 1px solid #eee;
                margin-right: 10px;
            }
        }
    </style> --}}
@endsection