@extends('associate.template.layout')

@section('title', 'Quick Support')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
             @if (session('errors'))
            <div class="alert alert-danger">
                @foreach (session('errors')->all() as $error)
                    <span class="text-center">{{ $error }}</span>
                @endforeach
            </div>
            @endif
        </div>
    </div>
	<div class="row">
        <div class="panel panel-primary panel-border">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Create Quick Support
                    <a href="{{ route('associate-quick-support') }}"> <button type="button" class="btn btn-inverse waves-effect w-xs waves-light pull-right">List of Quick Support</button></a>
                </h3>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <form role="form" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Category</label>
                            <select class="form-control" name="category" required>
                                <option value="">Select</option>
                                @foreach ($support_categories as $category)
                                    <option value="{{ $category }}">{{ $category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control" placeholder="Enter Subject" required>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Message</label>
                            <textarea id="elm1" name="message" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row text-center">
                        <button type="submit" class="btn btn-danger m-t-10">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

