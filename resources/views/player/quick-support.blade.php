@extends('associate.template.layout')

@section('title', 'Quick Support')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>
	<div class="row">
        <div class="panel panel-purple panel-border">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Your Quick Support List
                    <a href="{{ route('associate-quick-support-create') }}"> <button type="button" class="btn btn-inverse waves-effect w-xs waves-light pull-right">Add Quick Support</button></a>
                </h3>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="10%">Created On</th>
                                <th width="10%">Category</th>
                                <th width="20%">Subject</th>
                                <th width="35%">Message</th>
                                <th width="20%">Answer</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($supports as $index => $support)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ Carbon\Carbon::parse($support->created_at)->format('d-m-Y') }}</td>
                                    <td>{{ $support->category }}</td>
                                    <td>{{ $support->subject }}</td>
                                    <td><div class="message-box">{!! $support->message !!}</div></td>
                                    <td><div class="message-box">{{ $support->answer == null ? 'NA' : $support->answer }}</div></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $supports->links() }}
            </div>
        </div>
    </div>

@endsection

@section('page-css')
    <style type="text/css">
        .message-box
        {
            width: 100%;
            height: 60px;
            overflow-y: scroll;
            padding: 5px;
            border: 1px solid #eee;
        }
    </style>
@endsection