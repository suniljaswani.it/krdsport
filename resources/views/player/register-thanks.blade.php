<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Associate Login For Seyago India">
    <meta name="author" content="Seyago India">
    <link rel="shortcut icon" href="/website-assets/images/favicon.png">

    <title>Register Successfull - Seyago India</title>

    <link href="/associate-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="all">
        .account-pages {
            background: url(/associate-assets/images/register-bg.png)  repeat center;
            position: absolute;
            height: 100%;
            width: 100%;
        }
    </style>

    <script src="/associate-assets/js/modernizr.min.js"></script>

</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
    	<div class="m-t-40 card-box">
            <div class="text-center spacetb-10">
                @if (session('errors'))
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="text-center m-b-20">
                <a href="{{ route('associate-login') }}" class="logo">
                    <img src="/website-assets/images/seyago-logo.png" alt="">
                </a>
            </div>
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Thanks</h4>
                {{-- <span>Please let us know who introduced you to Seyago.</span> --}}
            </div>
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success"> {{ session('success') }}</div>
                @endif
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td><b>Username</b></td>
                                <td>{{ $user->username }}</td>
                            </tr>
                            <tr>
                                <td><b>Tracking ID</b></td>
                                <td>{{ $user->tracking_id }}</td>
                            </tr>
                            <tr>
                                <td><b>Password</b></td>
                                <td>{{ $user->password }}</td>
                            </tr>
                            <tr>
                                <td><b>Wallet Password</b></td>
                                <td>{{ $user->wallet_password }}</td>
                            </tr>

                        </table>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Go To <a href="{{ route('associate-login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card-box-->



    </div>
    <!-- end wrapper page -->

	<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/associate-assets/js/jquery.min.js"></script>
    <script src="/associate-assets/js/bootstrap.min.js"></script>
    <script src="/associate-assets/js/detect.js"></script>
    <script src="/associate-assets/js/fastclick.js"></script>
    <script src="/associate-assets/js/jquery.slimscroll.js"></script>
    <script src="/associate-assets/js/jquery.blockUI.js"></script>
    <script src="/associate-assets/js/waves.js"></script>
    <script src="/associate-assets/js/wow.min.js"></script>
    <script src="/associate-assets/js/jquery.nicescroll.js"></script>
    <script src="/associate-assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="/associate-assets/js/jquery.core.js"></script>
    <script src="/associate-assets/js/jquery.app.js"></script>

</body>
</html>