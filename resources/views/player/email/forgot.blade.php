<html>
<head>
    <meta charset="utf-8">
    <title>Reset Password Link</title>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>

    <table>
        <tr>
            <td><h3><span style="text-align: center;">Kindly Click on below link to reset password</span></h3></td>
        </tr>
        <tr>
            <td>
                <h3><a href="{{ route('associate-reset-password', ['id' => $user->id ,'key' => $user->remember_token]) }}" style="text-align: center;">Click here to Reset Password</a></h3>
            </td>
        </tr>
    </table>

</body>
</html>
