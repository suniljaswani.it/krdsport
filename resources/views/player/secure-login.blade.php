@extends('associate.template.layout')

@section('title', 'Wallet Password')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-danger panel-border">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Login For Payout and Pins
                    </h3>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <form role="form" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label>Wallet Password</label>
                                <input type="password" name="wallet_password" class="form-control" placeholder="Enter Wallet Password" required>
                            </div>
                        </div>
                        <div class="row text-center">
                            <button type="submit" class="btn btn-danger m-t-10">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

