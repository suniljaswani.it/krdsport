<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Associate Login For Seyago India">
    <meta name="author" content="Seyago India">
    <link rel="shortcut icon" href="/website-assets/images/favicon.png">

    <title>Register Step 1 - Seyago India</title>

    <link href="/associate-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="all">
        .account-pages {
            background: url(/associate-assets/images/register-bg.png)  repeat center;
            position: absolute;
            height: 100%;
            width: 100%;
        }
    </style>
    <script src="/associate-assets/js/modernizr.min.js"></script>
</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        {{-- <div class="text-center">
            <a href="{{ route('associate-login') }}" class="logo"><span>Seyago<span>India</span></span></a>
        </div> --}}
    	<div class="m-t-40 card-box">
            <div class="text-center spacetb-10">
                @if (session('errors'))
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="text-center m-b-20">
                <a href="{{ route('associate-login') }}" class="logo">
                    <img src="/website-assets/images/seyago-logo.png" alt="">
                </a>
            </div>
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Join Now</h4>
                <span>Please let us know who introduced you to Seyago.</span>
            </div>
            <div class="panel-body">
                {{--<div class="row">--}}
                    {{--<h3 class="alert alert-info">--}}
                        {{--Registration Process is Currently Under Maintenance.. we will be back soon..--}}
                    {{--</h3>--}}
                {{--</div>--}}
                <form class="form-horizontal m-t-20" method="post">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <div class="col-md-12">
                            <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="tracking_id" class="form-control" value="{{ old('tracking_id') }}" placeholder="Tracking ID" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <select name="package_id" class="form-control" required>
                                <option value disabled hidden selected>Select Package*</option>
                                @foreach ($packages as $package)
                                    <option value="{{ $package->id }}"  {{ old('package_id') == $package->id ? 'selected' : '' }} >
                                        {{ $package->name.' ('.$package->prefix.' = Rs.'.$package->amount.'/-'.')' }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="pin_number" value="{{ old('pin_number') }}" class="form-control" placeholder="Pin" required>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-30">
                        <div class="col-xs-12">
                            <button class="btn btn-danger btn-bordred btn-block waves-effect waves-light" type="submit">
                                Countinue <i class="fa fa-hand-o-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Already have an account? <a href="{{ route('associate-login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card-box-->



    </div>
    <!-- end wrapper page -->



	<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/associate-assets/js/jquery.min.js"></script>
    <script src="/associate-assets/js/bootstrap.min.js"></script>
    <script src="/associate-assets/js/detect.js"></script>
    <script src="/associate-assets/js/fastclick.js"></script>
    <script src="/associate-assets/js/jquery.slimscroll.js"></script>
    <script src="/associate-assets/js/jquery.blockUI.js"></script>
    <script src="/associate-assets/js/waves.js"></script>
    <script src="/associate-assets/js/wow.min.js"></script>
    <script src="/associate-assets/js/jquery.nicescroll.js"></script>
    <script src="/associate-assets/js/jquery.scrollTo.min.js"></script>

    <!-- App js -->
    <script src="/associate-assets/js/jquery.core.js"></script>
    <script src="/associate-assets/js/jquery.app.js"></script>

</body>
</html>