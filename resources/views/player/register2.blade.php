<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Associate Login For Seyago India">
    <meta name="author" content="Seyago India">
    <link rel="shortcut icon" href="/website-assets/images/favicon.png">

    <title>Register Step 2 - Seyago India</title>

    <link href="/associate-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/associate-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <style type="text/css" media="all">
        .account-pages {
            background: url(/associate-assets/images/register-bg.png)  repeat center;
            position: absolute;
            height: 100%;
            width: 100%;
        }
        .wrapper-page {
            margin: 2% auto;
            position: relative;
            width: 50%;
        }
        .spacetb-10{
            margin: 10px 0;
        }
        @media only screen and (max-width: 767px)
        {
            .wrapper-page {
                margin: 2% auto;
                position: relative;
                width: 90%;
            }
        }
    </style>
    <script src="/associate-assets/js/modernizr.min.js"></script>

</head>
<body>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
    	<div class="m-t-40 card-box">
            <div class="text-center spacetb-10">
                @if (session('errors'))
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="text-center m-b-20">
                <a href="{{ route('associate-login') }}" class="logo">
                    <img src="/website-assets/images/seyago-logo.png" alt="">
                </a>
            </div>
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Register Step 2</h4>
                <span class="text-danger">All Fields Marked * are required</span>
            </div>
            <div class="panel-body">
                <form class="form-group m-t-20" method="post">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <div class="spacetb-10">Tracking ID of Sponsor Upline</div>
                            <input type="text" value="{{ $sponsor->tracking_id }}" class="form-control" placeholder="Sponsor By*" required readonly >
                            <span class="help-block">
                                <small class="label label-success">{{ $sponsor->user_detail->title.' '.$sponsor->user_detail->first_name.' '.$sponsor->user_detail->last_name }}</small>
                            </span>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="spacetb-10">Tracking ID of Direct Upline*</div>
                            <input type="text" class="form-control upline_tracking_id" placeholder="Enter Upline Tracking ID*">
                            <input type="hidden" class="upline_id" name="upline_id">
                            <span class="help-block text-danger upline_name"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group leg_selection_radios">
                            <div class="spacetb-10">Select the Correct Organisation of your Upline where you wish to be placed.*</div>
                            <span class="l_block"><label class="radio-inline"><input type="radio" class="l_checked" name="leg" value="L" checked="checked">Left</label></span>
                            <span class="r_block"><label class="radio-inline"><input type="radio" class="r_checked" name="leg" value="R">Right</label></span>
                        </div>
                        <div class="col-md-12 no_space_leg_message_block hide">
                            <div class="spacetb-10 text-danger">There is No Legs available in this down..!!</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <select name="title" class="form-control" required>
                                <option value disabled hidden selected>Title*</option>
                                <option value="Mr" {{ old('title') == 'Mr' ? 'selected' : null }}>Mr</option>
                                <option value="Miss" {{ old('title') == 'Miss' ? 'selected' : null }}>Miss</option>
                                <option value="Mrs" {{ old('title') == 'Mrs' ? 'selected' : null }}>Mrs</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" placeholder="First Name*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" placeholder="Last Name*" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="input-group">
                                <input type="text" name="dob" value="{{ old('dob') }}" class="form-control" placeholder="Date of Birth*" id="datepicker-autoclose1" required>
                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email*" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="Mobile*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="address" value="{{ old('address') }}" class="form-control" placeholder="Address*" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="landmark" value="{{ old('landmark') }}" class="form-control" placeholder="Landmark*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="city" value="{{ old('city') }}" class="form-control" placeholder="City*" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="district" value="{{ old('district') }}" class="form-control" placeholder="District*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <select name="state" class="form-control" required>
                                <option value disabled hidden selected>State*</option>
                                @foreach ($states as $state)
                                    <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="number" min="1" name="pincode" value="{{ old('pincode') }}" class="form-control" placeholder="Pincode*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="pan_no" value="{{ old('pan_no') }}" class="form-control" placeholder="Pan No">
                            <span class="help-block">
                                <small class="label label-info">Pan Number is not required but you must update later. </small>
                            </span>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="nominee_name" value="{{ old('nominee_name') }}" class="form-control" placeholder="Nominee Name*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <select name="nominee_relation" class="form-control" required>
                                <option value disabled hidden selected>Nominee Relation*</option>
                                @foreach ($nominee_relation as $relation)
                                    <option value="{{ $relation }}" {{ old('nominee_relation') == $relation ? 'selected' : null }}>{{ $relation }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="input-group">
                                <input type="text" name="nominee_birth_date" value="{{ old('nominee_birth_date') }}" class="form-control" placeholder="Nominee Date of Birth*" id="datepicker-autoclose2" required>
                                <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b-30 m-t-20">
                        <div class="text-center">
                            <h4 class="text-uppercase font-bold m-b-0">Seyago Account Details</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password*" required>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="password" name="re_type_password" class="form-control" placeholder="Re-Type Password*" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox2" type="checkbox" name="terms" >
                                <label for="checkbox2">I agree to the <a href="javascript:void(0)">Terms and Conditions</a> and <a href="javascript:void(0)">Privacy Policy</a> For Seyago Services Pvt. Ltd.</label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-30">
                        <div class="form-group text-center col-md-4 col-md-offset-4">
                            <button class="btn btn-danger btn-bordred btn-block waves-effect waves-light" type="submit">
                                Countinue <i class="fa fa-hand-o-right"></i>
                            </button>
                        </div>
                    </div>

                </form>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Already have an account? <a href="{{ route('associate-login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card-box-->
    </div>
    <!-- end wrapper page -->

	<script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/associate-assets/js/jquery.min.js"></script>
    <script src="/associate-assets/js/bootstrap.min.js"></script>
    <script src="/associate-assets/js/detect.js"></script>
    <script src="/associate-assets/js/fastclick.js"></script>
    <script src="/associate-assets/js/jquery.slimscroll.js"></script>
    <script src="/associate-assets/js/jquery.blockUI.js"></script>
    <script src="/associate-assets/js/waves.js"></script>
    <script src="/associate-assets/js/wow.min.js"></script>
    <script src="/associate-assets/js/jquery.nicescroll.js"></script>
    <script src="/associate-assets/js/jquery.scrollTo.min.js"></script>

    <script src="/associate-assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- App js -->
    <script src="/associate-assets/js/jquery.core.js"></script>
    <script src="/associate-assets/js/jquery.app.js"></script>

    <script type="text/javascript">

        jQuery('#datepicker-autoclose1,#datepicker-autoclose2').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
            endDate: "-18y"
        });

        var leg_selection_radio = $('.leg_selection_radios');
        var no_empty_leg_message = $('.no_space_leg_message_block');
        var left_radio_block = $('.l_block');
        var right_radio_block = $('.r_block');

        $('.upline_tracking_id').change( function () {

            $.get('/associate/api/get-user', {'tracking_id': $(this).val()}, function (response) {

                no_empty_leg_message.addClass('hide');

                if(response.status == true) {

                    $('.upline_id').val(response.user_id);
                    $('.upline_name').html(response.name);

                    if(response.open_legs == 0) {
                        leg_selection_radio.addClass('hide');
                        no_empty_leg_message.removeClass('hide');
                    }
                    else if(response.open_legs === 'L') {
                        leg_selection_radio.removeClass('hide');
                        left_radio_block.removeClass('hide');
                        right_radio_block.addClass('hide');

                        $('.l_checked').attr('checked', true);
                        $('.r_checked').attr('checked', false);
                    }
                    else if(response.open_legs === 'R') {
                        leg_selection_radio.removeClass('hide');
                        left_radio_block.addClass('hide');
                        right_radio_block.removeClass('hide');

                        $('.r_checked').attr('checked', true);
                        $('.l_checked').attr('checked', false);
                    }
                    else if(response.open_legs === 2) {

                        leg_selection_radio.removeClass('hide');
                        left_radio_block.removeClass('hide');
                        right_radio_block.removeClass('hide');
                    }
                }
                else
                {
                    $('.upline_name').html(response.message);
                }

            });

        });



    </script>

</body>
</html>